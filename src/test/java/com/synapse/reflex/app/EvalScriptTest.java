package com.synapse.reflex.app;

import com.synapse.ms.things.logic.JSRuleScriptEngine;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.nodes.LogicNode;
import com.synapse.ms.things.model.nodes.RuleScript;
import com.synapse.ms.things.model.nodes.ThingDescription;
import org.junit.Before;
import org.junit.Test;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Properties;

public class EvalScriptTest {

    ScriptEngineManager mgr = new ScriptEngineManager();
    ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
    Registry<LogicNode> nodesRegistry;

    @Before
    public void setUp() {

        nodesRegistry = new Registry<>();

        try {
            Properties props = new Properties();
            props.setProperty("org.quartz.threadPool.threadCount","50");
            props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
            props.setProperty("org.quartz.scheduler.makeSchedulerThreadDaemon","true");

            new StdSchedulerFactory(props).getScheduler().start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testExecScript() {

        String nodeId = "uRule1";
        String ruleScript = "rule(function() {log('test constructor 1');" +
                "createCron(function() {log('cron 1');}, '0 */1 * ? * MON-FRI');" +
                "}, function() {" +
                "})";


        runScript(nodeId, ruleScript);

        /*try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        //nodeId = "uRule2";
        ruleScript = "rule(function() {log('test constructor 2');" +
                "createCron(function() {log('cron 2');}, '0 */2 * ? * MON-FRI');" +
                "}, function() {" +
                "})";
        runScript(nodeId, ruleScript);

        try {
            Thread.sleep(180000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void runScript(String nodeId, String ruleScript) {

        RuleScript node = (RuleScript) nodesRegistry.find(nodeId);

        if (node == null) {

            ThingDescription desc = new ThingDescription();
            desc.setGid(nodeId);
            desc.setScript(ruleScript);

            node = new RuleScript(desc.getGid());
            node.setNodeDescription(desc);

            nodesRegistry.register(node);

            JSRuleScriptEngine scriptEngine = new JSRuleScriptEngine(jsEngine,
                    null,
                    nodesRegistry,
                    null);

            node.setScriptEngine(scriptEngine);

            try {

                node.getScriptEngine().initialize();
                node.getScriptEngine().evalScript(node);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {


            try {

                node.getNodeDescription().setScript(ruleScript);
                node.getScriptEngine().reEvalScript(node);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
