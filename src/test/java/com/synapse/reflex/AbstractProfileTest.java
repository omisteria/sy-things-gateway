package com.synapse.reflex;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 12/7/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ActiveProfiles(profiles= { HSQL })
public abstract class AbstractProfileTest {

    /**
     * Setup the security context before each test.
     */
    @Before
    public void setUp() {
        doInit();
    }

    /**
     * Clear the security context after each test.
     */
    @After
    public void tearDown() {
        //SecurityContextHolder.clearContext();
    }

    /**
     * Set the default user on the security context.
     */
    protected abstract void doInit();

}
