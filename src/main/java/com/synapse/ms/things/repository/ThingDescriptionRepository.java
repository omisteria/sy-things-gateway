package com.synapse.ms.things.repository;

import com.synapse.ms.things.model.nodes.ThingDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by omist on 08.05.2017.
 */
@Repository
@Transactional
public interface ThingDescriptionRepository  extends JpaRepository<ThingDescription, Integer> {

    ThingDescription findThingDescriptionByGidEquals(String gid);
}
