package com.synapse.ms.things.repository;

import com.synapse.ms.things.model.nodes.NodeLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by omist on 09.07.2017.
 */
@Repository
@Transactional
public interface NodeLinkRepository extends JpaRepository<NodeLink, String> {

    @Query(value = "select c from NodeLink c where c.nodeId = :nodeId")
    List<NodeLink> findAllLinksOfNode(@Param("nodeId") String nodeId);

    @Query(value = "select c from NodeLink c where c.nodeId = :nodeId and c.observedId = :observedId")
    NodeLink findLinkOfNode(@Param("nodeId") String nodeId, @Param("observedId") String observedId);
}
