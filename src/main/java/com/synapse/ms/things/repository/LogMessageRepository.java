package com.synapse.ms.things.repository;

import com.synapse.ms.things.model.LogMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by omist on 18.06.2017.
 */
public interface LogMessageRepository extends JpaRepository<LogMessage, Integer> {

    @Query(value = "select * from logmessages order by create_time desc limit ?1", nativeQuery = true)
    List<LogMessage> findAllWithLimit(int limit);
}
