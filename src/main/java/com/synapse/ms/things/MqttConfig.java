package com.synapse.ms.things;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Configuration
@IntegrationComponentScan
@EnableAutoConfiguration
public class MqttConfig {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static long clientSuff = System.currentTimeMillis();

    @Value("${spring.server.mqtt.url}")
    private String mqttUrl;

    @Value("${spring.server.mqtt.client}")
    private String clientName;

    @Value("${spring.server.mqtt.qos:2}")
    private Integer mqttQos;

    // publish events to
    @Value("${spring.mqtt.topic.ithings-status}")
    private String ithingsStatusTopic;
    @Value("${spring.mqtt.topic.intercom}")
    private String intercomServiceTopic;

    // listening events from
    @Value("${spring.mqtt.topic.ithings-commands}")
    private String ithingsCommandsTopic;

    @Bean
    public MessageChannel thingsCommandInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel thingsStatusOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel intercomOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "thingsStatusOutboundChannel", autoStartup = "true")
    public MessageHandler thingsStatusOutboundHandler() {
        return makeHandler(ithingsStatusTopic);
    }

    @Bean
    @ServiceActivator(inputChannel = "intercomOutboundChannel", autoStartup = "true")
    public MessageHandler intercomOutboundHandler() {
        return makeHandler(intercomServiceTopic);
    }

    private MqttPahoMessageHandler makeHandler(String topic) {
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler(getClientName(), mqttClientFactory());

        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic(topic);
        messageHandler.setDefaultQos(mqttQos);
        messageHandler.setCompletionTimeout(50000);
        return messageHandler;
    }

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setServerURIs(new String[] { mqttUrl });
        factory.setUserName("guest");
        factory.setPassword("guest");
        return factory;
    }

    private String getClientName() {
        return clientName + "-" + clientSuff;
    }
}
