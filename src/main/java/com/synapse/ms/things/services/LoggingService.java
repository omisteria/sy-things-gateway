package com.synapse.ms.things.services;

import com.synapse.ms.things.model.LogMessage;
import com.synapse.ms.things.repository.LogMessageRepository;
import com.synapse.ms.things.web.model.DataType;
import com.synapse.ms.things.web.model.SocketEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by omist on 18.06.2017.
 */
@Component
public class LoggingService {

    @Autowired
    private LogMessageRepository logMessageRepository;

    @Autowired
    private SimpMessagingTemplate template;

    public void saveMessage(String message) {
        LogMessage logMessage = new LogMessage();
        logMessage.setMessage(message);
        logMessage.setUpdateTime(new Date());
        logMessageRepository.save(logMessage);

        template.convertAndSend("/topic/events", new SocketEvent(DataType.LOGGING_LIST));
    }

    public List<LogMessage> getLastMessages(int limit) {
        return logMessageRepository.findAllWithLimit(limit);
    }
}
