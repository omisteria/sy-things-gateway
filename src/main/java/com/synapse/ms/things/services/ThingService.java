package com.synapse.ms.things.services;

import com.synapse.ms.things.model.nodes.ThingDescription;
import com.synapse.ms.things.repository.ThingDescriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by omist on 08.05.2017.
 */
@Component
public class ThingService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ThingDescriptionRepository thingDescriptionRepository;

    public ThingService() {
    }

    public ThingDescription findThingDescriptionByGid(String gid) {
        return thingDescriptionRepository.findThingDescriptionByGidEquals(gid);
    }

    public List<ThingDescription> findAllThingDescriptions() {
        return thingDescriptionRepository.findAll();
    }

    public void saveNodeDescription(ThingDescription thingDescription) {
        thingDescriptionRepository.save(thingDescription);
    }

    public ThingDescription createRule(ThingDescription desc) {
        return desc;
    }
}
