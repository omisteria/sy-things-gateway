package com.synapse.ms.things.services;

import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.nodes.LogicNode;
import com.synapse.ms.things.model.nodes.NodeLink;
import com.synapse.ms.things.repository.NodeLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by omist on 09.07.2017.
 */
@Component
public class NodeLinkService {

    @Autowired
    private NodeLinkRepository nodeLinkRepository;

    @Autowired
    private Registry<LogicNode> nodesRegistry;


    public List<NodeLink> findAllLinks() {
        return nodeLinkRepository.findAll();
    }

    public List<NodeLink> findAllLinksOfNode(String nodeId) {
        return nodeLinkRepository.findAllLinksOfNode(nodeId);
    }

    public void save(NodeLink link) {
        nodeLinkRepository.save(link);
    }

    public NodeLink addLink(String nodeId, String observedId) {
        NodeLink link = nodeLinkRepository.findLinkOfNode(nodeId, observedId);

        if (link != null) {
            throw new IllegalStateException("Link already exists");
        }

        link = new NodeLink();
        link.setNodeId(nodeId);
        link.setObservedId(observedId);

        nodeLinkRepository.save(link);

        LogicNode node = nodesRegistry.find(nodeId);

        if (node == null) {
            throw new IllegalStateException("Node is not exists");
        }

        if (!node.getObservedNodesIds().contains(observedId)) {
            node.getObservedNodesIds().add(observedId);
        }

        LogicNode observer = nodesRegistry.find(observedId);
        if (observer != null && !observer.getObservesNodesIds().contains(nodeId)) {
            observer.getObservesNodesIds().add(nodeId);

            //node.addListener(observer); //???
            observer.addListener(node);
        }

        return link;
    }

    public NodeLink removeLink(String nodeId, String observedId) {
        NodeLink link = nodeLinkRepository.findLinkOfNode(nodeId, observedId);

        if (link == null) {
            throw new IllegalStateException("Link is not exists");
        }

        nodeLinkRepository.delete(link);

        LogicNode node = nodesRegistry.find(nodeId);

        if (node == null) {
            throw new IllegalStateException("Node is not exists");
        }

        if (node.getObservedNodesIds().contains(observedId)) {
            node.getObservedNodesIds().remove(observedId);
        }

        LogicNode observer = nodesRegistry.find(observedId);
        if (observer != null && observer.getObservesNodesIds().contains(nodeId)) {
            observer.getObservesNodesIds().remove(nodeId);

            node.removeListener(observer);
        }

        return link;
    }
}
