package com.synapse.ms.things.logic;

/**
 * Created by Igor on 6/6/2015.
 */
public class NodeModificationException extends Exception {

    public NodeModificationException() {
        super();
    }

    public NodeModificationException(String message) {
        super(message);
    }

    public NodeModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NodeModificationException(Throwable cause) {
        super(cause);
    }

    protected NodeModificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
