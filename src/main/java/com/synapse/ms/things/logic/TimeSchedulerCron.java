package com.synapse.ms.things.logic;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;

/**
 * Created by Igor on 5/7/2015.
 */
public class TimeSchedulerCron implements TimeScheduler {

    private String GROUP = "TIME_GROUP";

    private TriggerKey triggerKey;
    private JobKey jobKey;
    private JobDetail job;
    //private Scheduler scheduler;

    public TimeSchedulerCron() {
    }

    private Scheduler getCurrentScheduler() {
        try {

            return StdSchedulerFactory.getDefaultScheduler();

        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void schedule(String expression, HandlerJSTimeout handler) {
        jobKey = genJobKey();
        triggerKey = genTriggerKey();

        JobDataMap map = new JobDataMap();
        map.put("handler", handler);

        job = newJob(TimeSchedulerJob.class)
                .withIdentity(jobKey)
                .setJobData(map)
                .build();

        //CronExpression cexpr = new CronExpression(expression);
        //cexpr.getNextValidTimeAfter(new Date());

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(cronSchedule(expression))
                .build();

        try {
            getCurrentScheduler().scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public void reschedule(String expression) {

        try {
            Scheduler scheduler = getCurrentScheduler();
            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(jobKey);

            // new trigger key & new trigger
            triggerKey = genTriggerKey();

            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(triggerKey)
                    .forJob(job)
                    .withSchedule(cronSchedule(expression))
                    //.withSchedule(cronSchedule(expression).inTimeZone(TimeZone.getTimeZone("GMT")))
                    .build();

            // schedule job
            scheduler.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public void unschedule() {

        try {
            Scheduler scheduler = getCurrentScheduler();

            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(jobKey);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    private TriggerKey genTriggerKey() {
        String triggerIdentity = "TimeTrigger_" + System.currentTimeMillis() + "_" + hashCode();
        return new TriggerKey(triggerIdentity, GROUP);
    }

    private JobKey genJobKey() {
        String jobIdentity = "TimeJob_" + System.currentTimeMillis() + "_" + hashCode();
        return new JobKey(jobIdentity, GROUP);
    }
}
