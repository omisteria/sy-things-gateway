package com.synapse.ms.things.logic;

/**
 * Created by Igor on 5/5/2015.
 */
public abstract class HandlerNodeConstructor {

    public abstract void perform();
}
