package com.synapse.ms.things.logic;

import com.synapse.ms.things.model.nodes.LogicNode;

/**
 * Created by Igor on 5/5/2015.
 */
public abstract class HandlerJSNode {

    public abstract void perform(LogicNode emitter, Double oldValue, Double newValue);
}
