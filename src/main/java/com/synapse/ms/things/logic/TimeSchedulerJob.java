package com.synapse.ms.things.logic;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by Igor on 5/7/2015.
 */
public class TimeSchedulerJob implements Job
{
    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        HandlerJSTimeout handler = (HandlerJSTimeout) context.getJobDetail().getJobDataMap().get("handler");
        handler.perform();
    }

}
