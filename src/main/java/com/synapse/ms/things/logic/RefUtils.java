package com.synapse.ms.things.logic;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;
import jdk.nashorn.internal.objects.NativeDate;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class RefUtils {

    private static Location location = new Location("50.6195733", "26.2512302"); // Ukraine, Rivne
    private static SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, TimeZone.getDefault().getID()); // +0300

    public static Date getOfficialSunset() {
        return calculator.getOfficialSunsetCalendarForDate(Calendar.getInstance()).getTime();
    }

    public static Date getOfficialSunrise() {
        return calculator.getOfficialSunriseCalendarForDate(Calendar.getInstance()).getTime();

    }

    public static void main(String[] args) {

        //System.out.println(new Date().getTime());

        /*Calendar calendar = Calendar.getInstance();
        String officialSunrise = calculator.getOfficialSunriseForDate(calendar);
        String officialSunset = calculator.getOfficialSunsetForDate(calendar);
        Calendar sunriseCal = calculator.getOfficialSunriseCalendarForDate(calendar);
        Calendar sunsetCal = calculator.getOfficialSunsetCalendarForDate(calendar);

        System.out.println(officialSunrise+", "+officialSunset);
        System.out.println(sunriseCal.getTime());
        System.out.println(sunsetCal.getTime());*/
    }
}
