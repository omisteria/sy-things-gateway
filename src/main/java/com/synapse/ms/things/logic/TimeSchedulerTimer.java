package com.synapse.ms.things.logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Igor on 5/7/2015.
 */
public class TimeSchedulerTimer extends TimeSchedulerCron {

    @Override
    public void schedule(String expression, HandlerJSTimeout handler) {
        super.schedule(cronExpression(expression), handler);
    }

    @Override
    public void reschedule(String expression) {
        super.reschedule(cronExpression(expression));
    }

    private String cronExpression(String expression) {
        Pattern twopart = Pattern.compile("(\\d+):(\\d+)");

        Matcher m = twopart.matcher(expression);
        if (m.matches()) {
            return String.format("0 %s %s * * ? *", m.group(2), m.group(1));
        }

        return "";
    }
}
