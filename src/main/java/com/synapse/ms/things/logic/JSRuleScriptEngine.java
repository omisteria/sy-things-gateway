package com.synapse.ms.things.logic;

import com.google.common.eventbus.EventBus;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;
import com.synapse.ms.things.model.events.RequestIntercomEvent;
import com.synapse.ms.things.model.nodes.LogicNode;
import com.synapse.ms.things.model.nodes.RuleScript;
import com.synapse.ms.things.services.LoggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.io.*;
import java.util.*;

/**
 * Created by omist on 01.07.2017.
 */
public class JSRuleScriptEngine implements RuleScriptEngine {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private List<Timer> timers = new ArrayList<>();
    private List<TimeSchedulerCron> crons = new ArrayList<>();
    private Bindings bindings;
    private ScriptContext context;
    private ScriptEngine engine;
    //private RuleScript rule;
    private HandlerJSNode handler;

    private Registry<LogicNode> nodesRegistry;
    private EventBus nodesEventBus;
    private LoggingService loggingService;

    public JSRuleScriptEngine(ScriptEngine engine,
                              EventBus nodesEventBus,
                              Registry<LogicNode> nodesRegistry,
                              LoggingService loggingService) {
        this.engine = engine;
        this.nodesRegistry = nodesRegistry;
        this.nodesEventBus = nodesEventBus;
        this.loggingService = loggingService;

        bindings = engine.createBindings();

        context = new SimpleScriptContext();
        context.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
    }

    public void initialize() throws Exception {
        bindings.put("sys", this);
        nodesRegistry.getAllElements().forEach(node -> bindings.put(node.getId(), node));

        InputStream is = getClass().getClassLoader().getResourceAsStream("jsengine.js");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        engine.eval(bufferedReader, context);
    }

    public void handleEvent(LogicNodeChangedValueEvent event) {
        LogicNode emitter = event.getSource();
        Double oldValue = event.getOldValue();
        Double newValue = event.getNewValue();

        if (handler != null) {
            handler.perform(emitter, oldValue, newValue);
        } else {
            logger.warn("Without handler: emiter "+emitter.getId()+", value "+newValue);
        }
    }

    public void evalScript(RuleScript rule) throws ScriptException {
        //this.rule = rule;
        bindings.put("$rule", rule);
        bindings.put("$scope", new HashMap<String, Object>());

        /*engine.eval(String.format("sys.node('%s', function() {%s}, function() {%s});",
                rule.getDescription().getScriptInit(),
                rule.getDescription().getScriptMain()
        ), context);*/

        engine.eval(rule.getNodeDescription().getScript(), context);
    }

    public void reEvalScript(RuleScript rule) throws ScriptException {
        if (rule != null) {

            // clear setTimeout's
            for (Timer t : timers) {
                t.cancel();
            }
            timers.clear();

            for (TimeSchedulerCron t : crons) {
                t.unschedule();
            }
            // doesn't clear, TimeSchedulerCron stay in memory as container and can be relaunched

            evalScript(rule);
        }
    }


    // JS delegates

    public void registerTimer(Timer timer) {
        timers.add(timer);
    }

    public void sayText(String text) {
        if (nodesEventBus != null) {
            RequestIntercomEvent event = new RequestIntercomEvent(this, text);
            nodesEventBus.post(event);
        }
    }

    public void log(String message) {
        if (loggingService != null) {
            loggingService.saveMessage(message);
        } else {
            System.out.println(message);
        }
    }

    public void rule(HandlerNodeConstructor constructor, HandlerJSNode handler) {

        //rule.setHandler(handler);
        this.handler = handler;

        if (constructor != null) {
            constructor.perform();
        }
    }


    public TimeScheduler cron(HandlerJSTimeout handler, String expression) {
        TimeSchedulerCron timeScheduler = new TimeSchedulerCron();
        timeScheduler.schedule(expression, handler);
        crons.add(timeScheduler);

        return timeScheduler;
    }
    public void clearScheduler(TimeScheduler timeScheduler) {
        if (timeScheduler != null)
            timeScheduler.unschedule();
    }
    public TimeScheduler timepoint(HandlerJSTimeout handler, String expression) {
        TimeSchedulerTimer timeScheduler = new TimeSchedulerTimer();
        timeScheduler.schedule(expression, handler);

        return timeScheduler;
    }

    public Date sunrise() {
        return RefUtils.getOfficialSunrise();
    }

    public Date sunset() {
        return RefUtils.getOfficialSunset();
    }

}
