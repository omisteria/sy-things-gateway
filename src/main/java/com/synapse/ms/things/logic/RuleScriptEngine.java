package com.synapse.ms.things.logic;

import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;
import com.synapse.ms.things.model.nodes.RuleScript;

import javax.script.ScriptException;

public interface RuleScriptEngine {

    void evalScript(RuleScript rule) throws ScriptException;

    void reEvalScript(RuleScript rule) throws ScriptException;

    void initialize() throws Exception;

    void handleEvent(LogicNodeChangedValueEvent event);
}
