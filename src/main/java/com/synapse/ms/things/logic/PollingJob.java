package com.synapse.ms.things.logic;

import com.synapse.ms.things.model.nodes.Device;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * User: Igor
 * Date: 4/6/15
 * Time: 3:16 PM
 */
public class PollingJob implements Job {

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        Device device = (Device) jobExecutionContext.getJobDetail()
                .getJobDataMap().get("device");

        device.sendPollingRequest();
    }
}