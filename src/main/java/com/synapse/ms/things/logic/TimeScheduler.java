package com.synapse.ms.things.logic;

/**
 * Created by Igor on 5/7/2015.
 */
public interface TimeScheduler {

    void schedule(String expression, HandlerJSTimeout handler);

    void reschedule(String expression);

    void unschedule();

}
