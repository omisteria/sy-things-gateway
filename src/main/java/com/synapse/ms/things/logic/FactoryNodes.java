package com.synapse.ms.things.logic;

import com.google.common.eventbus.EventBus;
import com.synapse.ms.things.bridge.common.SysBridge;
import com.synapse.ms.things.bridge.ethvkmod.EthVKModBridge;
import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.bridge.rs485tcp.RS485TCPBridge;
import com.synapse.ms.things.bridge.sonoffhttp.SonoffHttpBridge;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.nodes.*;
import com.synapse.ms.things.services.LoggingService;
import com.synapse.ms.things.services.NodeLinkService;
import com.synapse.ms.things.services.ThingService;
import org.apache.commons.lang.StringUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.repeatSecondlyForever;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * User: Igor
 * Date: 4/5/15
 * Time: 8:26 PM
 */
@Component
public class FactoryNodes {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String THING_TYPE_SENSOR = "sensor";
    public static final String THING_TYPE_SWITCH = "switch";
    public static final String THING_TYPE_LOGIC = "logic";

    private static final String SCHEDULER_GROUP = "ThingRegistry";

    private Map<String, Bridge> bridgesReristry = new HashMap<String, Bridge>();

    //private Map<String, RuleScriptEngine> contextRegistry = new HashMap<>();

    @Autowired
    private LoggingService loggingService;

    @Autowired
    private ThingService thingService;

    @Autowired
    private NodeLinkService nodeLinkService;

    @Autowired
    private EventBus nodesEventBus;

    @Autowired
    private Registry<LogicNode> nodesRegistry;


    public FactoryNodes() {
    }

    public void addBridge(String id, Bridge bridge) {
        bridgesReristry.put(id, bridge);
    }

    public void activateAllBridges() {

        Bridge bridge;

        bridge = new SysBridge();
        bridgesReristry.put(bridge.getIdentificator(), bridge);

        bridge = new OwBridge();
        bridgesReristry.put(bridge.getIdentificator(), bridge);

        bridge = new RS485TCPBridge();
        bridgesReristry.put(bridge.getIdentificator(), bridge);

        bridge = new EthVKModBridge();
        bridgesReristry.put(bridge.getIdentificator(), bridge);

        bridge = new SonoffHttpBridge();
        bridgesReristry.put(bridge.getIdentificator(), bridge);

        for (Map.Entry<String, Bridge> entry : bridgesReristry.entrySet()) {
            Bridge bridge1 = entry.getValue();

            try {
                bridge1.activate();
            } catch (Exception e) {
                logger.warn("Failed bridge activation", e);
            }
        }
    }

    public void deactivateAllBridges() {
        for (Map.Entry<String, Bridge> entry : bridgesReristry.entrySet()) {
            Bridge bridge = entry.getValue();

            try {
                bridge.deactivate();
            } catch (Exception ignore) {}
        }
    }

    public Map<String, Bridge> getBridgesReristry() {
        return bridgesReristry;
    }

    public void createNodes(List<ThingDescription> thingDescriptions) {

        // briges & engines
        activateAllBridges();

        List<NodeLink> nodeLinks = nodeLinkService.findAllLinks();

        thingDescriptions.stream().forEach(desc -> createNode(desc));

        // set links between nodes
        setLinksBetweenNodes(nodeLinks);
    }

    public LogicNode createNode(ThingDescription desc) {

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");

        LogicNode node = null;

        if (THING_TYPE_SENSOR.equalsIgnoreCase(desc.getType())) {

            node = new SensorDevice(desc.getGid());

            Thing thing = (Thing)node;

            try {
                thing.setDevice(createDevice(desc.getDriverClassName(), desc.getOptions()));

                node.setInitialized(true);

            } catch (Exception e) {
                logger.warn("Create thing", e);
                node.setInitialized(false);
            }

        } else if (THING_TYPE_SWITCH.equalsIgnoreCase(desc.getType())) {

            node = new SwitchDevice(desc.getGid());

            Thing thing = (Thing)node;

            try {
                thing.setDevice(createDevice(desc.getDriverClassName(), desc.getOptions()));

                node.setInitialized(true);

            } catch (Exception e) {
                logger.warn("Create thing", e);
                node.setInitialized(false);
            }

        } else if (THING_TYPE_LOGIC.equalsIgnoreCase(desc.getType())) {

            node = new RuleScript(desc.getGid());

            try {
                JSRuleScriptEngine scriptEngine = new JSRuleScriptEngine(jsEngine, nodesEventBus, nodesRegistry, loggingService);

                ((RuleScript)node).setScriptEngine(scriptEngine);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        if (node != null) {

            node.setName(desc.getName());
            node.setNodeDescription(desc);
            node.setEventBus(nodesEventBus);
            node.setNodesRegistry(nodesRegistry);

            //final String nodeId = node.getId();
                        /*node.getObservedNodesIds().addAll(nodeLinks.stream()
                                .filter(link -> link.getNodeId().equals(nodeId))
                                .map(link -> link.getObservedId())
                                .collect(Collectors.toList()));*/

                        /*node.getObservesNodesIds().addAll(nodeLinks.stream()
                                .filter(link -> link.getObservedId().equals(nodeId))
                                .map(link -> link.getNodeId())
                                .collect(Collectors.toList()));*/

            logger.info(String.format("Add node to Register: %s", node.getId()));
            nodesRegistry.register(node);
        }

        return node;
    }

    public void setLinksBetweenNodes(final List<NodeLink> nodeLinks) {
        nodesRegistry.getAllElements().stream().forEach(node -> {

            final String nodeId = node.getId();

            nodeLinks.stream()
                    .filter(link -> link.getNodeId().equals(nodeId))
                    .forEach(link -> {
                        node.getObservedNodesIds().add(link.getObservedId());
                    });

            nodeLinks.stream()
                    .filter(link -> link.getObservedId().equals(nodeId))
                    .forEach(link -> {
                        node.getObservesNodesIds().add(link.getNodeId());

                        LogicNode observer = nodesRegistry.find(link.getNodeId());
                        if (observer != null) {
                            node.addListener(observer);
                        }
                    });
        });
    }

    private Device createDevice(String className, String options) throws Exception {

        boolean invert = false;
        int polling = 0;

        String opt[] = options.split("#");
        String bridgeId = opt[0];
        String deviceId = opt[1];
        String chanelId = null;
        if (opt.length >= 3) {
            chanelId = opt[2];
        }
        if (opt.length >= 4) {
            try {
                polling = Integer.valueOf(opt[3]);
            } catch (Exception e) {
            }
        }
        if (opt.length >= 5) {
            invert = !StringUtils.isEmpty(opt[4]) && "true".equalsIgnoreCase(opt[4]);
        }

        Bridge bridge = bridgesReristry.get(bridgeId);

        Constructor c = Class.forName(className).getConstructor(Bridge.class);
        Device device = (Device) c.newInstance(new Object[]{bridge});

        device.getProperties().setBridgeId(bridgeId);
        device.getProperties().setId(deviceId);
        device.getProperties().setChannelId(chanelId);
        device.getProperties().setInvert(invert);

        if (polling > 0) {
            device.getProperties().setPolling(polling);
            schedulePollingJob(device);
        }

        device.initialize();

        return device;

    }

    private void schedulePollingJob(Device device) {
        try {

            DeviceProperties dp = device.getProperties();
            String deviceIdentifier = dp.getBridgeId() + "." + dp.getId() + "." + dp.getChannelId() + "." + device.hashCode();

            Scheduler sched = StdSchedulerFactory.getDefaultScheduler();

            JobDataMap map = new JobDataMap();
            map.put("device", device);

            JobDetail job = newJob(PollingJob.class)
                    .withIdentity(new JobKey("Polling", SCHEDULER_GROUP + "." + deviceIdentifier))
                    .setJobData(map)
                    .build();

            SimpleTrigger trigger = newTrigger()
                    .withIdentity(new TriggerKey("Polling", SCHEDULER_GROUP + "." + deviceIdentifier))
                    .withSchedule(repeatSecondlyForever(dp.getPolling()))
                    .build();

            sched.scheduleJob(job, trigger);

            logger.debug("Scheduled polling job '{}' in DefaulScheduler, period='{}'", job.getKey(), dp.getPolling());

        } catch (SchedulerException e) {
            logger.warn("Could not schedule polling job: {}", e.getMessage());
        }
    }

    public void up() {

        try {

            //ScriptEngineManager mgr = new ScriptEngineManager();
            //ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");

            /*nodesRegistry.getAllElements()
                    .forEach(node -> {
                        jsEngine.put(node.getId(), node);
                    });*/

            nodesRegistry.getAllElements().stream()
                    .filter(node -> node instanceof RuleScript)
                    .forEach(logicNode -> {
                        try {
                            RuleScript ruleScript = (RuleScript)logicNode;

                            //JSRuleScriptEngine nodeScriptingContext = new JSRuleScriptEngine(jsEngine, nodesEventBus, nodesRegistry, loggingService);
                            //nodeScriptingContext.initialize();

                            ruleScript.getScriptEngine().initialize(); // rest of sensors and actuators must be are ready !!!!
                            ruleScript.getScriptEngine().evalScript(ruleScript);

                            //contextRegistry.put(ruleScript.getId(), ruleScript.getScriptEngine());

                            ruleScript.setInitialized(true);

                        } catch (Exception e) {
                            loggingService.saveMessage(String.format("Rule [%s] error: %s", logicNode.getId(), e.getMessage()));
                            logicNode.setInitialized(false);

                            e.printStackTrace();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateNodeScript(String nodeId, String script) {

        LogicNode logicNode = nodesRegistry.find(nodeId);

        if (logicNode != null) {
            RuleScript ruleScript = (RuleScript)logicNode;

            if (ruleScript.getId().equals(nodeId)) {

                RuleScriptEngine nodeScriptingContext = ruleScript.getScriptEngine();// contextRegistry.get(ruleScript.getId());
                if (nodeScriptingContext != null) {

                    /*nodesRegistry.getAllElements()
                            .stream()
                            .filter(node -> ruleScript.getObservedNodesIds().contains(node.getId()))
                            .forEach(node -> {
                                node.removeChangeListener(ruleScript);
                            });*/

                    try {
                        nodeScriptingContext.reEvalScript(ruleScript);

                        ThingDescription description = ruleScript.getNodeDescription();
                        description.setScript(script);
                        thingService.saveNodeDescription(description);

                        /*List<String> observesIds = ruleScript.getObservedNodesIds();
                        for (String nid : observesIds) {
                            LogicNode n = nodesRegistry.find(nid);
                            if (n != null)
                                n.addChangeListener(ruleScript);
                        }*/

                        loggingService.saveMessage(String.format("Rule [%s] has been updated", ruleScript.getId()));
                        ruleScript.setInitialized(true);

                    } catch (ScriptException e) {
                        e.printStackTrace();
                        ruleScript.setInitialized(false);
                        loggingService.saveMessage(String.format("Rule [%s] error: %s", ruleScript.getId(), e.getMessage()));

                        throw new IllegalStateException("Error: " + e.getMessage());
                    }
                }
            }
        }
    }

}
