package com.synapse.ms.things;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway()
public interface MqttGateway {

    @Gateway(requestChannel = "thingsStatusOutboundChannel")
    void publishThingStatus(Message message);

    @Gateway(requestChannel = "intercomOutboundChannel")
    void sayText(Message message);

}
