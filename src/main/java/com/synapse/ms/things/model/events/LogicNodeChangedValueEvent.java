package com.synapse.ms.things.model.events;

import com.synapse.ms.things.model.nodes.LogicNode;

import java.util.EventObject;

/**
 * Created by Igor on 5/10/2015.
 */
public class LogicNodeChangedValueEvent extends EventObject {

    private Double newValue;
    private Double oldValue;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public LogicNodeChangedValueEvent(LogicNode source, Double oldValue, Double newValue) {
        super(source);
        this.newValue = newValue;
        this.oldValue = oldValue;
    }

    @Override
    public LogicNode getSource() {
        return (LogicNode)super.getSource();
    }

    public Double getNewValue() {
        return newValue;
    }

    public void setNewValue(Double newValue) {
        this.newValue = newValue;
    }

    public Double getOldValue() {
        return oldValue;
    }

    public void setOldValue(Double oldValue) {
        this.oldValue = oldValue;
    }
}
