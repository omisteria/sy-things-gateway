package com.synapse.ms.things.model;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 12/3/14
 * Time: 12:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class UnitValue {

    public static final UnitValue LOW_LEVEL = valueOf(0);
    public static final UnitValue HIGH_LEVEL = valueOf(1);

    private String value = null;

    public static UnitValue undefined() {
        return new UnitValue(null);
    }

    public static UnitValue valueOf(String value) {
        return new UnitValue(value);
    }

    public static UnitValue valueOf(Double value) {
        if (value == null) {
            return undefined();
        } else {
            return new UnitValue(value.toString());
        }
    }

    public static UnitValue valueOf(Float value) {
        if (value == null) {
            return undefined();
        } else {
            return new UnitValue(value.toString());
        }
    }

    public static UnitValue valueOf(Integer value) {
        if (value == null) {
            return undefined();
        } else {
            return new UnitValue(value.toString());
        }
    }

    public static UnitValue valueOf(Boolean value) {
        if (value == null) {
            return undefined();
        } else {
            return new UnitValue(value ? "1" : "0");
        }
    }

    public boolean isHighLevel() {
        Integer v = getValueAsInteger();
        return  v > 0 && !v.equals(Integer.MIN_VALUE);
    }
    public boolean isLowLevel() {
        Integer v = getValueAsInteger();
        return v < 1  && !v.equals(Integer.MIN_VALUE);
    }
    public boolean isNull() {
        return value == null || (!isHighLevel() && !isLowLevel());
    }

    protected UnitValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Double getValueAsDouble() {
        return value != null ? Double.valueOf(value) : Double.MIN_VALUE;
    }

    public Integer getValueAsInteger() {
        return value != null ? Long.valueOf(Math.round(getValueAsDouble())).intValue() : Integer.MIN_VALUE;
    }

    public Boolean getValueAsBoolean() {
        return !getValueAsInteger().equals(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitValue unitValue = (UnitValue) o;

        if (value != null ? !value.equals(unitValue.value) : unitValue.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        if (value == null) return "undefined";
        return value;
    }
}
