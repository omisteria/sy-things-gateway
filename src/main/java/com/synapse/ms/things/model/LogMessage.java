package com.synapse.ms.things.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Igor on 5/10/2015.
 */
@Entity
@Table(name = "logmessages")
public class LogMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LOGMESSAGES")
    @SequenceGenerator(name = "SEQ_LOGMESSAGES", sequenceName = "SEQ_LOGMESSAGES", allocationSize = 1)
    @Column(name = "msg_id")
    private Long id;

    @Column(name = "create_time")
    private Date updateTime;

    private String message;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
