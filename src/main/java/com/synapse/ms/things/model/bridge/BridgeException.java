package com.synapse.ms.things.model.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/5/15
 * Time: 11:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class BridgeException extends Exception {

    public BridgeException() {
        super();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public BridgeException(String message) {
        super(message);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public BridgeException(String message, Throwable cause) {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public BridgeException(Throwable cause) {
        super(cause);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
