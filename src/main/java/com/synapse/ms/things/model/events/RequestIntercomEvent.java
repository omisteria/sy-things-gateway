package com.synapse.ms.things.model.events;

import java.util.EventObject;

/**
 * Created by omist on 03.07.2017.
 */
public class RequestIntercomEvent extends EventObject {

    private String text;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public RequestIntercomEvent(Object source, String text) {
        super(source);
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
