package com.synapse.ms.things.model.events;

/**
 * Created by omist on 22.07.2017.
 */
public interface LogicNodeEventListener {

    void handleEvent(LogicNodeChangedValueEvent event);

}
