package com.synapse.ms.things.model.events;

/**
 * Created by omist on 22.07.2017.
 */
public interface LogicNodeEventHandler {

    void addListener(LogicNodeEventListener listener);

    void removeListener(LogicNodeEventListener listener);

    void fireEvent(final LogicNodeChangedValueEvent resultSet);
}
