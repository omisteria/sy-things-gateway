package com.synapse.ms.things.model.nodes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.synapse.ms.things.logic.HandlerJSNode;
import com.synapse.ms.things.logic.RuleScriptEngine;
import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;

/**
 * Created by Igor on 5/5/2015.
 */
public class RuleScript extends AbstractRule {

    @JsonIgnore
    private RuleScriptEngine scriptEngine;

    //private HandlerJSNode handler;

    //private RuleDescription description;

    public RuleScript(String id) {
        super(id);
    }

    /*public HandlerJSNode getHandler() {
        return handler;
    }

    public void setHandler(HandlerJSNode handler) {
        this.handler = handler;
    }*/

    public String getType() {
        return "logic";
    }

    public RuleScriptEngine getScriptEngine() {
        return scriptEngine;
    }

    public void setScriptEngine(RuleScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
    }
/*@Override
    public String getName() {
        return description !=null ? description.getDescription() : null;
    }
*/
    /*@Override
    public void setName(String name) {

    }*/

    /*public RuleDescription getDescription() {
        return description;
    }*/

    /*public void setDescription(RuleDescription description) {
        this.description = description;
    }
*/
    public void handleEvent(LogicNodeChangedValueEvent event) {
        /*LogicNode emitter = event.getSource();
        Double oldValue = event.getOldValue();
        Double newValue = event.getNewValue();*/

        if (scriptEngine != null) {
            //System.out.println("perform " + getId());
            scriptEngine.handleEvent(event);
            //handler.perform(emitter, oldValue, newValue);
            //System.out.println("------f--" + System.currentTimeMillis());
        }
    }
}
