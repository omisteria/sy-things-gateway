package com.synapse.ms.things.model.events;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

import java.util.EventObject;

/**
 * Created by Igor on 6/7/2015.
 */
public abstract class EventHandler {

    @Subscribe
    @AllowConcurrentEvents
    public abstract void firedEvent(EventObject e);
}
