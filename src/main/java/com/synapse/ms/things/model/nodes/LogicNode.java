package com.synapse.ms.things.model.nodes;

import com.google.common.eventbus.EventBus;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.events.LogicNodeEventHandler;
import com.synapse.ms.things.model.events.LogicNodeEventListener;

import java.util.Date;
import java.util.List;

/**
 * User: Igor
 * Date: 4/28/15
 * Time: 4:33 AM
 */
public interface LogicNode extends LogicNodeEventListener, LogicNodeEventHandler {

    String getId();

    String getName();
    void setName(String name);

    Double getValue();

    void setValue(Double unitValue);

    String getType();

    void setEventBus(EventBus eventBus);

    boolean isInitialized();
    void setInitialized(boolean value);

    List<String> getObservedNodesIds();
    List<String> getObservesNodesIds();

    Date getTimeOfLastUpdate();
    void setTimeOfLastUpdate(Date date);

    ThingDescription getNodeDescription();
    void setNodeDescription(ThingDescription desc);

    Registry<LogicNode> getNodesRegistry();
    void setNodesRegistry(Registry<LogicNode> nodesRegistry);
}
