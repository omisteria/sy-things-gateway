package com.synapse.ms.things.model.nodes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/5/15
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDevice implements Device {

    private Device.ChangedDeviceValueHandler handler;

    private UnitValue unitValue = UnitValue.undefined();

    public void initialize() {

    }

    @JsonIgnore
    private Bridge bridge;

    private DeviceProperties properties = new DeviceProperties();

    protected AbstractDevice(Bridge bridge) {
        this.bridge = bridge;
    }

    public Bridge getBridge() {
        return bridge;
    }

    public DeviceProperties getProperties() {
        return properties;
    }

    public void setChangedValueHandler(Device.ChangedDeviceValueHandler handler) {
        this.handler = handler;
    }
    public void handleChangedValue(UnitValue value) {
        if (handler != null) {
            handler.handle(value);
        }
    }

    @Override
    public String getBridgeId() {
        return bridge.getIdentificator();
    }

    public void sendPollingRequest() {
        UnitValue oldValue = unitValue;

        try {
            unitValue = receiveValue();

            if (!oldValue.equals(unitValue)) {
                handleChangedValue(unitValue);
            }

        } catch (BridgeException e) {
            e.printStackTrace();
        }
    }
}
