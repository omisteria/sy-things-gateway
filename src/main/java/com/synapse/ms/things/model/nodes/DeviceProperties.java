package com.synapse.ms.things.model.nodes;

/**
 * User: Igor
 * Date: 4/5/15
 * Time: 9:06 PM
 */
public class DeviceProperties {

    private String bridgeId;

    private String id;

    private String channelId;

    private boolean invert;

    private int polling = 0;

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean isInvert() {
        return invert;
    }

    public void setInvert(boolean invert) {
        this.invert = invert;
    }

    public int getPolling() {
        return polling;
    }

    public void setPolling(int polling) {
        this.polling = polling;
    }
}
