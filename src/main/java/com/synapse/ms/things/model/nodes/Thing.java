package com.synapse.ms.things.model.nodes;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 11/29/14
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Thing extends LogicNode {

    Device getDevice();
    void setDevice(Device device);
}
