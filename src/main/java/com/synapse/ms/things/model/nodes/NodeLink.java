package com.synapse.ms.things.model.nodes;

import javax.persistence.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by omist on 09.07.2017.
 */
@Entity
@Table(name = "node_links")
public class NodeLink {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NODE_LINKS")
    @SequenceGenerator(name = "SEQ_NODE_LINKS", sequenceName = "SEQ_NODE_LINKS", allocationSize = 1)
    @Column(name = "link_id")
    private Long id;

    @Column(name = "node_id")
    private String nodeId;

    @Column(name = "observed_id")
    private String observedId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getObservedId() {
        return observedId;
    }

    public void setObservedId(String observedId) {
        this.observedId = observedId;
    }
}
