package com.synapse.ms.things.model;

import com.synapse.ms.things.model.nodes.LogicNode;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/20/15
 * Time: 3:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class Registry<T extends LogicNode> {

    private Map<String, T> registry = new HashMap<String, T>();

    public void register(T unit) {
        if (registry.containsKey(unit.getId())) return;

        registry.put(unit.getId(), unit);
    }
    public void registerAll(List<T> units) {
        for (T unit : units) {
            register(unit);
        }
    }

    public T find(String id) {

        if (id == null) return null;

        return registry.get(id);
    }

    public Collection<T> getAllElements() {
        return registry.values();
    }

    public void clearAllElements() {
        registry.clear();
    }

    public int size() {
        return registry.size();
    }

}
