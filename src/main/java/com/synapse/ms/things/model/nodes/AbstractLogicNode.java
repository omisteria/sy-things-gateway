package com.synapse.ms.things.model.nodes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;
import com.synapse.ms.things.model.events.LogicNodeEventHandler;
import com.synapse.ms.things.model.events.LogicNodeEventListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * User: Igor
 * Date: 4/28/15
 * Time: 5:00 AM
 */
public abstract class AbstractLogicNode implements LogicNode {

    private String id;

    private String name;

    private Double value;

    @JsonIgnore
    private EventBus eventBus;

    private boolean initialized;

    private List<String> observedNodesIds = new ArrayList();
    private List<String> observesNodesIds = new ArrayList();

    private Date timeOfLastUpdate;

    private ThingDescription nodeDescription;

    @JsonIgnore
    private Registry<LogicNode> nodesRegistry;

    private List<LogicNodeEventListener> listeners;


    public AbstractLogicNode(String id) {
        this.id = id;
        listeners = new ArrayList<LogicNodeEventListener>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {

        if (value == null) return;

        Double oldValue = this.value;

        this.timeOfLastUpdate = new Date();
        this.value = value;

        if (!value.equals(oldValue)) {
            fireEvent(new LogicNodeChangedValueEvent(this, oldValue, value));
        }
    }


    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean value) {
        initialized = value;
    }

    
    public List<String> getObservedNodesIds() {
        return observedNodesIds;
    }

    public List<String> getObservesNodesIds() {
        return observesNodesIds;
        /* return listeners.stream()
                .map(l -> ((LogicNode)l).getId())
                .collect(Collectors.toList()); */
    }


    @Override
    public void addListener(LogicNodeEventListener listener) {
        listeners.add(listener);
    }

    @Override
    public abstract void handleEvent(LogicNodeChangedValueEvent event);

    @Override
    public void removeListener(LogicNodeEventListener listener) {
        for (int i = listeners.size() - 1; i >= 0; i--) {
            LogicNodeEventListener instance = listeners.get(i);
            if (instance.equals(listener)) {
                listeners.remove(i);
            }
        }
    }

    @Override
    public void fireEvent(LogicNodeChangedValueEvent event) {

        if (eventBus != null)
            new Thread(() -> eventBus.post(event)).start();

        for (int i = 0; i < listeners.size(); i++) {
            final LogicNodeEventListener instance = listeners.get(i);
            Runnable runnable = () -> instance.handleEvent(event);
            new Thread(runnable).start();
        }
    }

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
        //this.eventBus.register(this);
    }

    public Registry<LogicNode> getNodesRegistry() {
        return nodesRegistry;
    }

    public void setNodesRegistry(Registry<LogicNode> nodesRegistry) {
        this.nodesRegistry = nodesRegistry;
    }

    @Override
    public Date getTimeOfLastUpdate() {
        return timeOfLastUpdate;
    }

    @Override
    public void setTimeOfLastUpdate(Date date) {
        timeOfLastUpdate = date;
    }

    @Override
    public String toString() {
        return "$"+getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractLogicNode that = (AbstractLogicNode) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public ThingDescription getNodeDescription() {
        return nodeDescription;
    }

    @Override
    public void setNodeDescription(ThingDescription nodeDescription) {
        this.nodeDescription = nodeDescription;
    }
}
