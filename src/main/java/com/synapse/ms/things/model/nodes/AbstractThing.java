package com.synapse.ms.things.model.nodes;


import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.BridgeException;


/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 11/29/14
 * Time: 6:53 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractThing extends AbstractLogicNode implements Thing {

    private Device device;
    private Object sync = new Object();

    public AbstractThing(String id) {
        super(id);
    }


    public Double getValue() {
        synchronized (sync) {
            return super.getValue();
        }
    }

    @Override
    public void setValue(Double value) {

        if (device != null) {

            synchronized (sync) {
                try {

                    device.sendValue(UnitValue.valueOf(value));

                    // TODO: use feedback value
                    super.setValue(value);

                } catch (BridgeException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;

        device.setChangedValueHandler(value -> {
            if (value != null && !value.isNull()) {
                AbstractThing.super.setValue(value.getValueAsDouble());
            }
        });
    }
}
