package com.synapse.ms.things.model.nodes;

import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;

import java.util.List;

/**
 * Date: 11/30/14
 * Time: 11:52 PM
 */
public class SwitchDevice extends AbstractThing {

    public SwitchDevice(String id) {
        super(id);
        super.setValue(0.);
    }

    public String getType() {
        return "handler";
    }

    public void handleEvent(LogicNodeChangedValueEvent event) {
        LogicNode emitter = event.getSource();
        Double oldValue = event.getOldValue();
        Double newValue = event.getNewValue();

        // summarize value
        final boolean highLevelSignal[] = new boolean[] {false};
        final List<String> observedNodesIds = getObservedNodesIds();

        if (!observedNodesIds.isEmpty()) {

            highLevelSignal[0] = true;

            observedNodesIds.forEach(observed -> {
                LogicNode node = getNodesRegistry().find(observed);

                if (node != null && node.getValue() != null) {
                    highLevelSignal[0] = node.getValue() > 0 && highLevelSignal[0];
                } else {
                    highLevelSignal[0] = false;
                }

            });
        }

        sendValue(highLevelSignal[0] ? 1.0 : 0);
    }

    private synchronized void sendValue(Double val) {

        setValue(val);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setValue(val);
    }

}
