package com.synapse.ms.things.model.nodes;

import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.BridgeException;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/5/15
 * Time: 8:38 PM
 */
public interface Device {

    void initialize();

    String getBridgeId();

    DeviceProperties getProperties();

    void setChangedValueHandler(ChangedDeviceValueHandler handler);
    void handleChangedValue(UnitValue value);

    UnitValue receiveValue() throws BridgeException;
    void sendValue(UnitValue value) throws BridgeException;

    void sendPollingRequest();

    interface ChangedDeviceValueHandler {
        void handle(UnitValue value);
    }
}
