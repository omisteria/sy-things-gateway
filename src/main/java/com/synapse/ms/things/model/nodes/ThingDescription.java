package com.synapse.ms.things.model.nodes;


import javax.persistence.*;

/**
 * Created by omist on 08.05.2017.
 */
@Entity
@Table(name = "thing_description")
public class ThingDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_THING_DESCRIPTION")
    @SequenceGenerator(name = "SEQ_THING_DESCRIPTION", sequenceName = "SEQ_THING_DESCRIPTION", allocationSize = 1)
    @Column(name = "descriptor_id")
    private Integer id;

    private String gid;

    private String name;

    private String type;

    private String options;

    @Column(name = "java_class")
    private String driverClassName;

    private String script;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
