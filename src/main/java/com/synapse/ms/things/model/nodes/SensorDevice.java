package com.synapse.ms.things.model.nodes;

import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;
import com.synapse.ms.things.model.nodes.AbstractThing;

/**
 * User: Igor
 * Date: 4/26/15
 * Time: 12:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class SensorDevice extends AbstractThing {

    public SensorDevice(String id) {
        super(id);
    }

    @Override
    public void handleEvent(LogicNodeChangedValueEvent event) {
        // doesn't need implementation
    }

    public String getType() {
        return "sensor";
    }
}
