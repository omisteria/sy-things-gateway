package com.synapse.ms.things.model.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/5/15
 * Time: 9:51 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Bridge {

    String getIdentificator();

    void activate();

    void deactivate();
}
