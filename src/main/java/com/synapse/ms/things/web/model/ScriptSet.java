package com.synapse.ms.things.web.model;

/**
 * Created by Igor on 6/28/2015.
 */
public class ScriptSet {

    private String devicesConfig;

    private String logicConfig;

    public String getDevicesConfig() {
        return devicesConfig;
    }

    public void setDevicesConfig(String devicesConfig) {
        this.devicesConfig = devicesConfig;
    }

    public String getLogicConfig() {
        return logicConfig;
    }

    public void setLogicConfig(String logicConfig) {
        this.logicConfig = logicConfig;
    }
}
