package com.synapse.ms.things.web.model;

/**
 * Created by omist on 30.06.2017.
 */
public enum DataType {

    LOGGING_LIST,
    LOGGING_MESSAGE,
    NODE

}
