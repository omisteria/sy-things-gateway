package com.synapse.ms.things.web.controllers;

import com.synapse.ms.things.model.LogMessage;
import com.synapse.ms.things.services.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Igor on 5/10/2015.
 */
@Controller
@RequestMapping("/api/v1/log")
public class LoggerController {

    @Autowired
    private LoggingService loggingService;

    @RequestMapping(value = "/all/{records}", method = RequestMethod.GET)
    @ResponseBody
    public List<LogMessage> getAllNodes(@PathVariable Integer records) {
        return loggingService.getLastMessages(records);
    }
}
