package com.synapse.ms.things.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Created by omist on 30.06.2017.
 */
public class SocketEventSerializer extends StdSerializer {

    public SocketEventSerializer() {
        super(SocketEvent.class);
    }

    public SocketEventSerializer(Class t) {
        super(t);
    }

    @Override
    public void serialize(Object o, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException {

        SocketEvent event = (SocketEvent)o;

        generator.writeStartObject();
        generator.writeFieldName("type");
        generator.writeString(event.getType().name());
        generator.writeEndObject();
    }
}
