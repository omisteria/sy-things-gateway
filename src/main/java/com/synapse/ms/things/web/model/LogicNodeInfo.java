package com.synapse.ms.things.web.model;

/**
 * Created by Igor on 5/10/2015.
 */
public class LogicNodeInfo {

    private String id;

    private String type;

    private String description;

    private Double value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
