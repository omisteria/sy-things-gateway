package com.synapse.ms.things.web.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by omist on 30.06.2017.
 */
@JsonSerialize(using = SocketEventSerializer.class)
public class SocketEvent {

    private DataType type;

    public SocketEvent(DataType type) {
        this.type = type;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }
}
