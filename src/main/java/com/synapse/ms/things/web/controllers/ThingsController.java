package com.synapse.ms.things.web.controllers;

import com.synapse.ms.things.logic.FactoryNodes;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.nodes.LogicNode;
import com.synapse.ms.things.model.nodes.NodeLink;
import com.synapse.ms.things.model.nodes.ThingDescription;
import com.synapse.ms.things.services.NodeLinkService;
import com.synapse.ms.things.services.ThingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omist on 12.05.2017.
 */
@Controller
@RequestMapping("/api/v1/nodes")
public class ThingsController {

    @Autowired
    private ThingService thingService;

    @Autowired
    private FactoryNodes factoryNodes;

    @Autowired
    private Registry<LogicNode> nodesRegistry;

    @Autowired
    private NodeLinkService nodeLinkService;

    @ResponseBody
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Collection<LogicNode> getAllThins() {
        return nodesRegistry.getAllElements();
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}", method = RequestMethod.GET)
    public LogicNode getNode(@PathVariable String nodeId) {
        return nodesRegistry.find(nodeId);
    }


    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}", method = RequestMethod.POST)
    public LogicNode createNode(@PathVariable String nodeId) {

        ThingDescription thingDescription = thingService.findThingDescriptionByGid(nodeId);
        if (thingDescription != null) {
            throw new IllegalStateException("Already exists");
        }

        thingDescription = new ThingDescription();
        thingDescription.setType(FactoryNodes.THING_TYPE_LOGIC);
        thingDescription.setGid(nodeId);
        thingDescription.setName("New Rule");
        thingDescription.setScript("rule(\n" +
                " function() {},\n" +
                " function() {}\n" +
                ")");
        thingDescription.setOptions("");
        thingDescription.setDriverClassName("com.synapse.ms.things.model.nodes.RuleScript");

        thingService.createRule(thingDescription);

        return factoryNodes.createNode(thingDescription);
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}", method = RequestMethod.PUT)
    public LogicNode updateNode(@PathVariable String nodeId, @RequestBody LogicNode inNode) {

        LogicNode logicNode = nodesRegistry.find(nodeId);
        if (logicNode == null) {
            throw new IllegalStateException("Node doesn't exists");
        }

        ThingDescription thingDescription = thingService.findThingDescriptionByGid(nodeId);
        if (thingDescription == null) {
            throw new IllegalStateException("Node description doesn't exists");
        }

        thingDescription.setName(inNode.getName());
        thingService.saveNodeDescription(thingDescription);

        logicNode.setName(inNode.getName());

        return logicNode;
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/value", method = RequestMethod.GET)
    public Map<String, String> getNodeValue(@PathVariable String nodeId) {

        LogicNode logicNode = nodesRegistry.find(nodeId);
        Assert.notNull(logicNode, "Node is null");

        Map<String, String> resp = new HashMap();
        resp.put("id", logicNode.getId());
        resp.put("value", Double.toString(logicNode.getValue()));

        return resp;
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/value/{value}", method = RequestMethod.POST)
    public LogicNode setNodeValue(@PathVariable String nodeId,
                                  @PathVariable Double value) {

        LogicNode logicNode = nodesRegistry.find(nodeId);
        Assert.notNull(logicNode, "Node is null");

        logicNode.setValue(value);

        return logicNode;
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/script", method = RequestMethod.POST)
    public void updateRuleScript(
            @PathVariable String nodeId,
            @RequestBody String script) {

        factoryNodes.updateNodeScript(nodeId, script);
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/addlink/{observedId}", method = RequestMethod.PUT)
    public NodeLink addNodeLink(
            @PathVariable String nodeId,
            @PathVariable String observedId) {

        return nodeLinkService.addLink(nodeId, observedId);
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/removelink/{observedId}", method = RequestMethod.PUT)
    public NodeLink removeNodeLink(
            @PathVariable String nodeId,
            @PathVariable String observedId) {

        return nodeLinkService.removeLink(nodeId, observedId);
    }

    @ResponseBody
    @RequestMapping(value = "/one/{nodeId}/nodelinks", method = RequestMethod.GET)
    public List<NodeLink> getNodeLinksOfNode(@PathVariable String nodeId) {
        return nodeLinkService.findAllLinksOfNode(nodeId);
    }
}
