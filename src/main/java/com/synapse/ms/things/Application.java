package com.synapse.ms.things;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.synapse.ms.things.logic.FactoryNodes;
import com.synapse.ms.things.model.Registry;
import com.synapse.ms.things.model.events.RequestIntercomEvent;
import com.synapse.ms.things.model.nodes.LogicNode;
import com.synapse.ms.things.model.events.EventHandler;
import com.synapse.ms.things.model.events.LogicNodeChangedValueEvent;
import com.synapse.ms.things.model.nodes.SensorDevice;
import com.synapse.ms.things.services.LoggingService;
import com.synapse.ms.things.services.ThingService;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.PostConstruct;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableAutoConfiguration
//@EnableScheduling
@ComponentScan(basePackages = "com.synapse.ms.things")
public class Application {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EventBus nodesEventBus;

    @Autowired
    private FactoryNodes factoryNodes;

    @Autowired
    private ThingService thingService;

    @Autowired
    private LoggingService loggingService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private Registry<LogicNode> nodesRegistry;

    @Value("${synapse.ms.intercom.server}")
    private String intercomServer;

    @Autowired
    private MqttGateway mqttGateway;

    public static void main(String[] args) {
        System.setProperty("spring.devtools.livereload.enabled", "true");
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void start() {

        logger.info("Run application ...");
        loggingService.saveMessage("Run application ...");

        // load & create things
        factoryNodes.createNodes(thingService.findAllThingDescriptions());

        logger.info(String.format("Loaded %d nodes", nodesRegistry.getAllElements().size()) );
        loggingService.saveMessage(String.format("Loaded %d nodes", nodesRegistry.getAllElements().size()) );

        factoryNodes.up();

        // get data of all sensors
        nodesRegistry.getAllElements().forEach(node -> {
            if (node instanceof SensorDevice) {
                final SensorDevice sensor = (SensorDevice)node;

                new Thread(() -> {
                    if (sensor.getDevice() != null) {
                        sensor.getDevice().sendPollingRequest();
                    }
                }).start();
            }
        });

        nodesEventBus.register(new EventHandler() {

            @Override
            public void firedEvent(EventObject e) {

                if (e instanceof LogicNodeChangedValueEvent) {
                    LogicNodeChangedValueEvent event = (LogicNodeChangedValueEvent)e;
                    LogicNode node = event.getSource();

                    logger.info("Node changed state [" + node.getId() + "]=" + event.getNewValue());

                    // save message to local log
                    loggingService.saveMessage("Node changed state [" + node.getId() + "]=" + event.getNewValue());

                    // Send message to thing-status queue
                    try {
                        String ident = node.getId();
                        String value = event.getNewValue() == null ? "0" : event.getNewValue().toString();

                        Map<String, String> messageMap = new HashMap();
                        messageMap.put("sender", "thingsgateway");
                        messageMap.put("thingId", ident);
                        messageMap.put("thingType", node.getType());
                        messageMap.put("thingDescription", node.getNodeDescription().getName());
                        messageMap.put("status", value);
                        String payload = new ObjectMapper().writeValueAsString(messageMap);
                        mqttGateway.publishThingStatus(MessageBuilder.withPayload(payload).build());

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    // Send message to websocket
                    template.convertAndSend("/topic/nodevalue", node);
                }
                else if (e instanceof RequestIntercomEvent) {

                    RequestIntercomEvent event = (RequestIntercomEvent)e;

                    // Send to Intercom queue
                    try {
                        Map<String, String> messageMap = new HashMap();
                        messageMap.put("sender", "thingsgateway");
                        messageMap.put("action", "saytext");
                        messageMap.put("volume", "100");
                        messageMap.put("chanel", "r1111");
                        messageMap.put("text", event.getText());
                        String payload = new ObjectMapper().writeValueAsString(messageMap);
                        mqttGateway.sayText(MessageBuilder.withPayload(payload).build());

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    /*try {
                        String url = intercomServer + "/api/saytext?scope=r1111&volume=100&text=" + URLEncoder.encode(event.getText(), "UTF-8");

                        Thread sendCommand = new Thread(() -> {

                            final String USER_AGENT = "Mozilla/5.0";

                            try {

                                HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                                con.setRequestMethod("GET");
                                con.setRequestProperty("User-Agent", USER_AGENT);

                                int responseCode = con.getResponseCode();
                                logger.info("Response from INTERCOM service: "+responseCode);

                            } catch (MalformedURLException e1) {
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        });
                        sendCommand.start();

                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                        logger.error(e2.getMessage());
                    }*/

                }
            }
        });

        try {

            Properties props = new Properties();
            props.setProperty("org.quartz.threadPool.threadCount","50");
            props.setProperty("org.quartz.scheduler.skipUpdateCheck","true");
            props.setProperty("org.quartz.scheduler.makeSchedulerThreadDaemon","true");

            new StdSchedulerFactory(props).getScheduler().start();

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Bean
    public EventBus nodesEventBus() {
        return new EventBus("nodes");
    }

    @Bean
    public Registry<LogicNode> nodesRegistry() {
        return new Registry<>();
    }

    /*@Bean
    public SimpMessagingTemplate brokerMessagingTemplate() {
        SimpMessagingTemplate template = new SimpMessagingTemplate(new MessageChannel() );
        template.setUserDestinationPrefix("/app");
        return template;
    }*/

/*    @Bean
    public EventBus applicationEventBus() {
        return new EventBus("application");
    }*/
}
