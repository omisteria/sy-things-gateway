package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created by Igor on 7/26/2015.
 */
public class SensorDS2408 extends AbstractDevice {


    public SensorDS2408(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/sensed." + getProperties().getChannelId();
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        UnitValue retValue = UnitValue.valueOf(bridge.readValue(getPath()));

        if (getProperties().isInvert()) {
            retValue = UnitValue.valueOf(retValue.getValueAsDouble() >0 ? 0. : 1.);
        }

        return retValue;
    }

    public void sendValue(UnitValue value) {
        //
    }
}
