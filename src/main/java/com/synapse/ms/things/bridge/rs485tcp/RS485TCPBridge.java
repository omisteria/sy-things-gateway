package com.synapse.ms.things.bridge.rs485tcp;

import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import net.wimpi.modbus.io.ModbusRTUTCPTransaction;
import net.wimpi.modbus.msg.*;
import net.wimpi.modbus.net.RTUTCPMasterConnection;
import net.wimpi.modbus.procimg.InputRegister;
import net.wimpi.modbus.util.BitVector;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Igor on 11/15/2015.
 */
public class RS485TCPBridge implements Bridge {

    private String hostname = "192.168.3.170";
    private int port = Integer.parseInt("9761");

    private RTUTCPMasterConnection connection;

    @Override
    public String getIdentificator() {
        return "rs485tcp";
    }

    public void activate() {
        try {
            if (connection == null)
                connection = new RTUTCPMasterConnection(InetAddress.getByName(hostname), port);
        } catch (UnknownHostException e) {
            connection = null;
        }

        if (!connection.isConnected()) {
            try {
                connection.connect();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void deactivate() {

        if (connection != null && connection.isConnected()) {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public InputRegister[] readInputRegister(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusRTUTCPTransaction netTransaction = new ModbusRTUTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadInputRegistersRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            ReadInputRegistersResponse responce = (ReadInputRegistersResponse)netTransaction.getResponse();
            return responce.getRegisters();

        } catch (Exception e) {
            //e.printStackTrace();
            throw new BridgeException("Read problem input register for device "+idDevice, e);
        }
    }

    public BitVector readInputDiscretes(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusRTUTCPTransaction netTransaction = new ModbusRTUTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadInputDiscretesRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            ReadInputDiscretesResponse responce = (ReadInputDiscretesResponse)netTransaction.getResponse();
            return responce.getDiscretes();

        } catch (Exception e) {
            //e.printStackTrace();
            throw new BridgeException("Read problem input discretes for device "+idDevice, e);
        }
    }

    public BitVector readCoils(int idDevice, int dataAddress, int dataAddressLength) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusRTUTCPTransaction netTransaction = new ModbusRTUTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new ReadCoilsRequest(dataAddress, dataAddressLength);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            ReadCoilsResponse responce = (ReadCoilsResponse)netTransaction.getResponse();
            return responce.getCoils();

        } catch (Exception e) {
            //e.printStackTrace();
            throw new BridgeException("Read problem coils for device "+idDevice, e);
        }
    }

    public boolean writeCoil(int idDevice, int dataAddress, boolean state) throws BridgeException {

        try {

            if (!connection.isConnected()) connection.connect();

            ModbusRTUTCPTransaction netTransaction = new ModbusRTUTCPTransaction();
            netTransaction.setConnection(connection);
            netTransaction.setReconnecting(false);

            ModbusRequest request = new WriteCoilRequest(dataAddress, state);
            request.setUnitID(idDevice);

            netTransaction.setRequest(request);

            try {
                netTransaction.execute();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            WriteCoilResponse responce = (WriteCoilResponse)netTransaction.getResponse();
            return responce != null ? responce.getCoil() : false;

        } catch (Exception e) {
            e.printStackTrace();
            throw new BridgeException("Write problem coil for device "+idDevice, e);
        }
    }

    public void writeValue(int idDevice, Double value) throws BridgeException {
        if (value == null) return;

        throw new IllegalStateException("Not implemented");
    }
}
