package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 11/29/14
 * Time: 10:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class TempSensorDS1820 extends AbstractDevice {

    public TempSensorDS1820(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/temperature";
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        UnitValue retValue = UnitValue.valueOf(bridge.readValue(getPath()));

        if (retValue != null && retValue.getValueAsDouble()>80) {
            throw new BridgeException("Incorrect device value");
        }

        return retValue;
    }

    public void sendValue(UnitValue value) {
        //
    }
}
