package com.synapse.ms.things.bridge.rs485tcp.device;

import com.synapse.ms.things.bridge.rs485tcp.RS485TCPBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;
import net.wimpi.modbus.procimg.InputRegister;

import java.nio.ByteBuffer;

/**
 * Created by Igor on 11/15/2015.
 */
public class InputRegisterSMD220 extends AbstractDevice {

    public InputRegisterSMD220(Bridge bridge) {
        super(bridge);
    }


    public UnitValue receiveValue() throws BridgeException {
        RS485TCPBridge bridge = (RS485TCPBridge)getBridge();

        try {
            int deviceId = Integer.valueOf(getProperties().getId());
            int dataAddress = Integer.valueOf(getProperties().getChannelId());

            InputRegister[] inputRegisters = bridge.readInputRegister(deviceId, dataAddress, 2);

            int index = 0;
            ByteBuffer buff = ByteBuffer.allocate(4);
            buff.put(inputRegisters[index * 2 + 0].toBytes());
            buff.put(inputRegisters[index * 2 + 1].toBytes());

            float val = ByteBuffer.wrap(buff.array()).getFloat();
            if( val > 0 && val < 0.0000001) val = 0;

            return UnitValue.valueOf(val);

        } catch (Exception e) {
            throw new BridgeException("Can't get value of device ID:"+getProperties().getId());
        }
    }


    public void sendValue(UnitValue value) throws BridgeException {

    }
}
