package com.synapse.ms.things.bridge.common;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Igor on 11/15/2015.
 */
public class ConnectionChecker {

    private Socket socket;
    private boolean tryToReconnect = true;
    private Thread heartbeatThread;
    private long heartbeatDelayMillis = 5000;

    private String host;
    private int port;



    public ConnectionChecker(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() {

        heartbeatThread = new Thread() {
            public void run() {
                connect();
                while (tryToReconnect) {
                    //send a test signal
                    try {

                        if (!checkConnectionHandler()) {

                            // try to connect
                            connect();

                            // if ok - disconnect & success handler
                        } else {

                        }

                        sleep(heartbeatDelayMillis);
                    } catch (InterruptedException e) {
                        // You may or may not want to stop the thread here
                        // tryToReconnect = false;
                    //} catch (IOException e) {
                        //connect();
                    }
                }
            }
        };
        heartbeatThread.start();
    }

    public boolean checkConnectionHandler() {
        try {
            socket.getOutputStream().write(666);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private void connect(){
        try {
            socket = new Socket(host, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        tryToReconnect = false;

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
