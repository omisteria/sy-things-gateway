package com.synapse.ms.things.bridge.rs485tcp.device;

import com.synapse.ms.things.bridge.rs485tcp.RS485TCPBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;
import net.wimpi.modbus.util.BitVector;

import java.nio.ByteBuffer;

/**
 * Created by Igor on 10/4/2016.
 */
public class CoilVKM extends AbstractDevice {

    public CoilVKM(Bridge bridge) {
        super(bridge);
    }

    public UnitValue receiveValue() throws BridgeException {
        RS485TCPBridge bridge = (RS485TCPBridge)getBridge();

        try {
            int deviceId = Integer.valueOf(getProperties().getId());
            int dataAddress = Integer.valueOf(getProperties().getChannelId());

            BitVector inputDiscretes = bridge.readCoils(deviceId, dataAddress, 1);

            ByteBuffer buff = ByteBuffer.allocate(4);
            buff.put(inputDiscretes.getBytes()[0]);

            return UnitValue.valueOf((int)ByteBuffer.wrap(buff.array()).get());

        } catch (Exception e) {
            throw new BridgeException("Can't get value of device ID:"+getProperties().getId());
        }
    }

    public void sendValue(UnitValue value) throws BridgeException {
        RS485TCPBridge bridge = (RS485TCPBridge)getBridge();

        try {
            int deviceId = Integer.valueOf(getProperties().getId());
            int dataAddress = Integer.valueOf(getProperties().getChannelId());

            boolean res = bridge.writeCoil(deviceId, dataAddress, value.getValueAsBoolean());

        } catch (Exception e) {
            throw new BridgeException(e);
        }
    }

}
