package com.synapse.ms.things.bridge.ethvkmod;

/**
 * Created by Igor on 3/20/2016.
 */
public interface VKModResponse {

    void receive(byte[] message);
}
