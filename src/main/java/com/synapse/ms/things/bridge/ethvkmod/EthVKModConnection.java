package com.synapse.ms.things.bridge.ethvkmod;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor on 3/20/2016.
 */
public class EthVKModConnection {

    private Socket socket;
    private int socketTimeout = 3000;

    private String ipAddress;
    private int ipPort;

    private boolean connected;
    private Thread  connectingTread;

    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    private boolean tryToReconnect = true;
    private long heartbeatDelayMillis = 5000;
    private long checkConnectionMillis = 12000;

    private List<VKModResponse> listeners = new ArrayList<VKModResponse>();

    public EthVKModConnection(String ipAddress, int ipPort, boolean autoReconect) {
        this.ipAddress = ipAddress;
        this.ipPort = ipPort;

        if (autoReconect) {
            Thread t = new Thread(new Runnable() {
                public void run() {
                    while(tryToReconnect) {

                        try {
                            Thread.sleep(checkConnectionMillis);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        //send a test signal
                        try {

                            sendMessage(666);
                            Thread.sleep(heartbeatDelayMillis);

                        } catch (InterruptedException e) {
                            tryToReconnect = false;
                        } catch (Exception e) {
                            try {
                                connected = false;
                                connect();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }

                    }
                }
            });
            t.start();
        }
    }

    public synchronized void connect() throws Exception {

        connectingTread = new Thread(new Runnable() {
            public void run() {
                try {
                    tryConnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        connectingTread.start();

    }// connect

    private void tryConnect() throws Exception {
        if (!this.connected)
        {
            System.out.println();
            System.out.println("connect to: "+this.ipAddress+", "+this.ipPort);

            // create a socket towards the remote slave
            this.socket = new Socket(this.ipAddress, this.ipPort);

            // set the socket timeout
            setTimeout(this.socketTimeout);

            connected = true;
            System.out.println("successfully connected");

            this.inputStream = new DataInputStream(socket.getInputStream());
            this.outputStream = new DataOutputStream(socket.getOutputStream());

            try
            {
                while(connected) {
                    int bytesRead = 0;
                    byte[] inStream = new byte[10025];

                    try {
                        bytesRead = inputStream.read(inStream);
                    } catch (IOException e) {

                    }

                    if (bytesRead > 0) {
                        for (VKModResponse listener : listeners) {
                            listener.receive(inStream);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                System.err.println("Error while reading from socket: " + e);
                // wrap and re-throw
                throw new IOException("I/O exception - failed to read.\n" + e);
            } finally {
                tryClose();
            }
        } else {
            System.out.println();
            System.out.println("already connected to: "+this.ipAddress+", "+this.ipPort);
        }
    }

    public void addResponseListener(VKModResponse vkModResponse) throws IOException
    {
        listeners.add(vkModResponse);
    }

    public void sendMessage(byte[] outStream) throws IOException {
        //if (this.connected) {
            outputStream.write(outStream);
        //}
    }
    public void sendMessage(int outStream) throws IOException {
        //if (this.connected) {
            outputStream.write(outStream);
        //}
    }

    public void close() {
        tryClose();
        tryToReconnect = false;
    }

    public void tryClose() {

        System.out.println("close connection, status:" + connected);

        // if connected... disconnect, otherwise do nothing
        if (this.connected)
        {
            // try closing the transport...
            try
            {
                if (this.socket != null && !this.socket.isClosed()) {
                    this.socket.close();
                }
            }
            catch (IOException e)
            {
                System.out.println("error while closing the connection, cause:" + e);
            }

            // if everything is fine, set the connected flag at false
            this.connected = false;
        }

    }// tryClose

    public void setTimeout(int timeout)
    {
        //store the current socket timeout
        this.socketTimeout = timeout;

        //set the timeout on the socket, if available
        if (this.socket != null)
        {
            try
            {
                this.socket.setSoTimeout(socketTimeout);
            }
            catch (IOException ex)
            {
                // TODO: handle?
            }
        }
    }// setReceiveTimeout


    public boolean isConnected() {
        //if (socket != null) return socket.isConnected();
        return connected;
    }
}
