package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created by Igor on 5/10/2015.
 */
public class TempSensorDS2438 extends AbstractDevice {

    public TempSensorDS2438(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/temperature";
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        return UnitValue.valueOf(bridge.readValue(getPath()));
    }

    public void sendValue(UnitValue value) throws BridgeException {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
