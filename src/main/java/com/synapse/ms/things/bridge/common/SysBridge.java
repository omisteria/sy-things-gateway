package com.synapse.ms.things.bridge.common;

import com.synapse.ms.things.model.bridge.Bridge;

/**
 * Created by omist on 14.05.2017.
 */
public class SysBridge implements Bridge {

    @Override
    public String getIdentificator() {
        return "sys";
    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }
}
