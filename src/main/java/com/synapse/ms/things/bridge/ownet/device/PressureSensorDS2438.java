package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/26/15
 * Time: 2:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class PressureSensorDS2438 extends AbstractDevice {

    public PressureSensorDS2438(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/B1-R1-A/pressure";
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        UnitValue retValue = UnitValue.valueOf(bridge.readValue(getPath()));

        if (retValue != null) {
            retValue = UnitValue.valueOf(Math.round(retValue.getValueAsDouble() * 0.75006375541921 * 10) / 10.);
        }

        return retValue;
    }

    public void sendValue(UnitValue value) throws BridgeException {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
