package com.synapse.ms.things.bridge.ethvkmod.device;

import com.synapse.ms.things.bridge.ethvkmod.EthVKModBridge;
import com.synapse.ms.things.bridge.ethvkmod.EthVKModConnection;
import com.synapse.ms.things.bridge.ethvkmod.VKModResponse;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

import java.io.IOException;

/**
 * Created by Igor on 3/20/2016.
 */
public class SensorSocket2 extends AbstractDevice {

    private UnitValue unitValue = UnitValue.undefined();
    private EthVKModConnection connection;

    public SensorSocket2(Bridge bridge) {
        super(bridge);
    }

    @Override
    public void initialize() {
        super.initialize();

        final int channel = Integer.parseInt(getProperties().getChannelId());

        connection = ((EthVKModBridge)getBridge()).getConnection(getProperties().getId());

        if (connection != null) {
            try {
                connection.addResponseListener(new VKModResponse() {
                    public void receive(byte[] message) {

                        if (message[0] == 0x21 && message[1] == channel) {

                            UnitValue retValue;

                            // invert of invert :)
                            if (getProperties().isInvert()) {
                                retValue = UnitValue.valueOf((int) message[2]);
                            } else {
                                retValue = UnitValue.valueOf(message[2] == 1 ? 0 : 1);
                            }

                            if (retValue != null) {
                                unitValue = retValue;
                                handleChangedValue(unitValue);
                            }
                        }

                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public UnitValue receiveValue() throws BridgeException {
        return unitValue;
    }

    public void sendValue(UnitValue value) throws BridgeException {

    }
}
