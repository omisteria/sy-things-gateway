package com.synapse.ms.things.bridge.common;

/**
 * Created by Igor on 11/15/2015.
 */
public interface CheckConnectionHandler {

    boolean perform();
}
