package com.synapse.ms.things.bridge.ownet;

import com.synapse.ms.things.bridge.ownet.device.TempSensorDS1820;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.Device;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/5/15
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Runner {

    public void start() {

        OwBridge bridge = new OwBridge();
        bridge.activate();

        Device temp = new TempSensorDS1820(bridge);
        temp.getProperties().setId("28.5B1372020000");

        UnitValue unitValue = null;
        try {

            unitValue = temp.receiveValue();
            System.out.println("v="+unitValue.getValueAsDouble());

        } catch (BridgeException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        new Runner().start();
    }
}
