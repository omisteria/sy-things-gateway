package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 12/8/14
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class HumiditySensorDS2438 extends AbstractDevice {

    public HumiditySensorDS2438(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/humidity";
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        return UnitValue.valueOf(bridge.readValue(getPath()));
    }

    public void sendValue(UnitValue value) {
        //
    }

}
