package com.synapse.ms.things.bridge.sonoffhttp;

import com.synapse.ms.things.model.bridge.Bridge;

/**
 * http://192.168.3.181/#
 * ESPURNA_A7B3DF
 * APIKEY C512895219827F46
 *
 * http://192.168.3.182/#
 * ESPURNA_A7A488
 * APIKEY CEFA8F83114B7C86
 *
 * http://192.168.3.181/api/relay/0?apikey=C512895219827F46
 * 192.168.3.181,C512895219827F46
 */
public class SonoffHttpBridge implements Bridge {

    @Override
    public String getIdentificator() {
        return "sonoff";
    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }
}
