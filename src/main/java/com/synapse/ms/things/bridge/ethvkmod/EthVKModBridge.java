package com.synapse.ms.things.bridge.ethvkmod;

import com.synapse.ms.things.model.bridge.Bridge;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by Igor on 11/15/2015.
 */
public class EthVKModBridge implements Bridge {

    private ConcurrentHashMap<String, EthVKModConnection> connections = new ConcurrentHashMap<String, EthVKModConnection>();

    /*private boolean tryToReconnect = true;
    private Thread heartbeatThread;
    private long heartbeatDelayMillis = 5000;*/

    @Override
    public String getIdentificator() {
        return "ethVKMod";
    }

    public void activate() {
        //System.out.println();
        //System.out.println("activate ...");

        /*for (Map.Entry<String, EthVKModConnection> entry : connections.entrySet()) {

            final EthVKModConnection conn = entry.getValue();
            if (conn != null && !conn.isConnected()) {

                Thread t = new Thread(new Runnable() {
                    public void run() {
                        //send a test signal
                        try {

                            conn.sendMessage(666);
                            Thread.sleep(heartbeatDelayMillis);

                        } catch (InterruptedException e) {
                            tryToReconnect = false;
                        } catch (Exception e) {
                            try {
                                conn.connect();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });
                t.start();
            }
        }

        heartbeatThread = new Thread() {
            public void run() {
                while (tryToReconnect) {

                    System.out.println();
                    System.out.println("check connections ...");


                }
            };
        };
        heartbeatThread.start();*/
    }

    public void deactivate() {
        //tryToReconnect = false;

        for (Map.Entry<String, EthVKModConnection> entry : connections.entrySet()) {

            EthVKModConnection conn = entry.getValue();
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public EthVKModConnection getConnection(String host, int port) {

        EthVKModConnection connection = connections.get(host);

        if (connection == null) {
            try {
                connection = new EthVKModConnection(host, port, true);
                connection.setTimeout(3000);
                connection.connect();

                connections.put(host, connection);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return connection;
    }

    public EthVKModConnection getConnection(String host) {
        return getConnection(host, 9761);
    }
}
