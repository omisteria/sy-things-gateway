package com.synapse.ms.things.bridge.common.device;

import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by omist on 14.05.2017.
 */
public class OneHourPeriod  extends AbstractDevice {

    private Timer timer;
    private long timeout = 600000;
    private UnitValue unitValue = UnitValue.undefined();

    public OneHourPeriod(Bridge bridge) {
        super(bridge);

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run()
            {
                if (unitValue.isLowLevel()) {
                    unitValue = UnitValue.HIGH_LEVEL;
                    handleChangedValue(unitValue);
                } else {
                    unitValue = UnitValue.LOW_LEVEL;
                    handleChangedValue(unitValue);
                }

            }

        }, 0, timeout);
    }

    @Override
    public UnitValue receiveValue() throws BridgeException {
        return unitValue;
    }

    @Override
    public void sendValue(UnitValue value) throws BridgeException {

    }
}
