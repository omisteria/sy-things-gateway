package com.synapse.ms.things.bridge.ownet.device;


import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 12/1/14
 * Time: 3:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class DoubleSwitchDS2413 extends AbstractDevice {

    public DoubleSwitchDS2413(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/" + getProperties().getId() + "/PIO." + getProperties().getChannelId();
    }


    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        return UnitValue.valueOf(bridge.readValue(getPath()));
    }

    public void sendValue(UnitValue value) throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        if (value.getValueAsInteger() == 0 || value.getValueAsInteger() == 1 ) {
            bridge.writeValue(getPath(), value.getValue());
        }
    }
}
