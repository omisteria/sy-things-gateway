package com.synapse.ms.things.bridge.ownet;


import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.Device;
import org.apache.log4j.Logger;
import org.owfs.jowfsclient.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 11/29/14
 * Time: 5:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class OwBridge implements Bridge {

    private static Logger log = Logger.getLogger(OwBridge.class);

    private Map<String, Device> devices = new HashMap<String, Device>();

    private String owfsHostname = "192.168.3.11";
    private int owfsPort = 4304;

    private OwfsConnection client;

    private void tryToReadAndLogPathValue(String subdir) throws IOException {
        try {
            System.out.println("\t" + subdir + "\t:" + client.read(subdir));
        } catch (OwfsException e) {
            System.out.println("\t" + subdir + "\t: DIRECTORY");
        }
    }

    @Override
    public String getIdentificator() {
        return "ow";
    }

    public void activate() {

        OwfsConnectionConfig connectionConfig = new OwfsConnectionConfig(owfsHostname, owfsPort);
        connectionConfig.setDeviceDisplayFormat(Enums.OwDeviceDisplayFormat.F_DOT_I);
        connectionConfig.setTemperatureScale(Enums.OwTemperatureScale.CELSIUS);
        connectionConfig.setPersistence(Enums.OwPersistence.ON);
        connectionConfig.setBusReturn(Enums.OwBusReturn.ON);

        client = OwfsConnectionFactory.newOwfsClientThreadSafe(connectionConfig);
        //ConnectionChecker connectionChecker = new ConnectionChecker(owfsHostname, owfsPort);
        //connectionChecker.start();

        //OwfsConnectionFactory owfsConnectionFactory = new OwfsConnectionFactory(owfsHostname, owfsPort);
        //client = owfsConnectionFactory.createNewConnection();

        /*
        List<String> directories = null;
        try {
            directories = client.listDirectory("/");

            for (String dir : directories) {

                System.out.println("DIR-> " + dir);


            }
        } catch (OwfsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
    }

    public void deactivate() {

        try {
            if (client!=null) client.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readValue(String path) throws BridgeException {
        try {
            return client.read(path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BridgeException("Read problem for path "+path, e);
        } catch (OwfsException e) {
            e.printStackTrace();
            throw new BridgeException("Read problem for path "+path, e);
        }
    }

    public void writeValue(String path, String value) throws BridgeException {
        if (value == null) return;

        try {
            client.write(path, value);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BridgeException("Write problem for path "+path, e);
        } catch (OwfsException e) {
            e.printStackTrace();
            throw new BridgeException("Write problem for path "+path, e);
        }
    }
}
