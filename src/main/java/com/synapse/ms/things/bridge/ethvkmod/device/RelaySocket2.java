package com.synapse.ms.things.bridge.ethvkmod.device;

import com.synapse.ms.things.bridge.ethvkmod.EthVKModBridge;
import com.synapse.ms.things.bridge.ethvkmod.EthVKModConnection;
import com.synapse.ms.things.bridge.ethvkmod.VKModResponse;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

import java.io.IOException;

/**
 * Created by Igor on 3/20/2016.
 */
public class RelaySocket2 extends AbstractDevice {

    private UnitValue unitValue = UnitValue.undefined();
    private EthVKModConnection connection;

    public RelaySocket2(Bridge bridge) {
        super(bridge);
    }

    public void initialize() {
        super.initialize();

        final int channel = Integer.parseInt(getProperties().getChannelId());

        connection = ((EthVKModBridge)getBridge()).getConnection(getProperties().getId());

        if (connection != null) {
            try {
                connection.addResponseListener(new VKModResponse() {
                    public void receive(byte[] message) {

                        if (message[0] == 0x22 && message[1] == channel) {
                            UnitValue retValue = UnitValue.valueOf((int) message[2]);
                            if (retValue != null) unitValue = retValue;
                        }

                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public UnitValue receiveValue() throws BridgeException {
        return unitValue;
    }

    public void sendValue(UnitValue value) throws BridgeException {

        final int channel = Integer.parseInt(getProperties().getChannelId());

        byte[] outStream = new byte[4];
        outStream[0] = 0x22;

        outStream[1] = (byte)channel; // channel

        int swValue = value.getValueAsInteger() > 0 ? 1 : 0;
        outStream[2] = (byte)swValue; // 1-on, 0-off

        outStream[3] = (byte)0;

        try {
            if (connection.isConnected())
                connection.sendMessage(outStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
