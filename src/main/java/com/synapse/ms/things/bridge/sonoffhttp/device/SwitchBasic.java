package com.synapse.ms.things.bridge.sonoffhttp.device;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapse.ms.things.bridge.sonoffhttp.SonoffHttpBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;
import org.springframework.http.HttpStatus;
//import org.apache.http.HttpStatus;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class SwitchBasic extends AbstractDevice {


    public SwitchBasic(Bridge bridge) {
        super(bridge);
    }

    @Override
    public UnitValue receiveValue() throws BridgeException {
        SonoffHttpBridge bridge = (SonoffHttpBridge)getBridge();

        try {

            String ipAndApi = getProperties().getId(); // IP,APIKEY
            String host = ipAndApi.split(",")[0];
            String apikey = ipAndApi.split(",")[1];

            StringBuilder sb = new StringBuilder();
            sb.append("http://")
                    .append(host)
                    .append("/api/relay/")
                    .append(getProperties().getChannelId())
                    .append("?apikey=")
                    .append(apikey);

            //http://192.168.1.108/api/relay/0?apikey=C62ED7BE7593B658

            HttpURLConnection con = (HttpURLConnection) new URL(sb.toString()).openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept", "application/json");

            int responseCode = con.getResponseCode();
            if (responseCode == HttpStatus.OK.value()) {

                StringBuilder insb = new StringBuilder();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) insb.append(inputLine);
                in.close();

                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(insb.toString());
                //{ "relay0": 0}
                JsonNode valNode = root.get("relay"+getProperties().getChannelId());
                UnitValue.valueOf(valNode.asInt());
            }

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return null;
    }

    /**
     * There are restrictions on the outgoing request from the Docker container as this is between different networks.
     * @param value
     * @throws BridgeException
     */
    @Override
    public void sendValue(UnitValue value) throws BridgeException {
        SonoffHttpBridge bridge = (SonoffHttpBridge)getBridge();

        try {

            //192.168.1.108,C62ED7BE7593B658#0
            String ipAndApi = getProperties().getId(); // IP,APIKEY
            String host = ipAndApi.split(",")[0];
            String apikey = ipAndApi.split(",")[1];

            StringBuilder sb = new StringBuilder();
            sb.append("http://").append(host).append("/api/relay/").append(getProperties().getChannelId());
            String url = sb.toString();

            sb = new StringBuilder();
            sb
                    .append("apikey=")
                    .append(URLEncoder.encode(apikey, "UTF-8"))
                    .append("&value=")
                    .append(value.getValueAsBoolean() ? 1 : 0);
            String reqData = sb.toString();

            HttpURLConnection con = sendRequest(url, reqData);

            int responseCode = con.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new BridgeException("Set value error, incorrect response from device [url: "+url+"," +
                        " data: "+reqData+"," +
                        " code="+responseCode+"," +
                        " message="+con.getResponseMessage()+"]");
            }

            try(BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()))) {
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            throw new BridgeException("Set value error", e1);
        }
    }

    private HttpURLConnection sendRequest(String url, String data) throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

        byte[] reqData = data.getBytes( StandardCharsets.UTF_8 );

        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Cache-Control", "no-cache");
        con.setDoOutput(true);
        con.setUseCaches(false);

        try(DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.write( reqData );
        }

        return con;
    }

    /*public static void main(String[] args) throws IOException {
        HttpURLConnection con = new SwitchBasic(null)
                .sendRequest("http://192.168.3.186/api/relay/0", "apikey=99AD674BA59A1E56&value=0");

        System.out.println(con.getResponseCode());
    }*/
}
