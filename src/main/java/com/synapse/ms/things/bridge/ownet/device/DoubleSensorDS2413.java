package com.synapse.ms.things.bridge.ownet.device;

import com.synapse.ms.things.bridge.ownet.OwBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 4/26/15
 * Time: 12:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class DoubleSensorDS2413 extends AbstractDevice {

    public DoubleSensorDS2413(Bridge bridge) {
        super(bridge);
    }

    private String getPath() {
        return "/uncached/" + getProperties().getId() + "/sensed." + getProperties().getChannelId();
    }

    public UnitValue receiveValue() throws BridgeException {
        OwBridge bridge = (OwBridge)getBridge();

        UnitValue retValue = UnitValue.valueOf(bridge.readValue(getPath()));

        if (getProperties().isInvert()) {
            retValue = UnitValue.valueOf((retValue.getValueAsDouble() >0 ? 0. : 1.));
        }

        return retValue;
    }

    public void sendValue(UnitValue value) {
        //
    }
}
