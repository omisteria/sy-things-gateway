package com.synapse.ms.things.bridge.rs485tcp.device;

import com.synapse.ms.things.bridge.rs485tcp.RS485TCPBridge;
import com.synapse.ms.things.model.UnitValue;
import com.synapse.ms.things.model.bridge.Bridge;
import com.synapse.ms.things.model.bridge.BridgeException;
import com.synapse.ms.things.model.nodes.AbstractDevice;
import net.wimpi.modbus.util.BitVector;

import java.nio.ByteBuffer;

/**
 * Created by Igor on 10/4/2016.
 */
public class DiscreteInputVKM extends AbstractDevice {

    public DiscreteInputVKM(Bridge bridge) {
        super(bridge);
    }


    public UnitValue receiveValue() throws BridgeException {
        RS485TCPBridge bridge = (RS485TCPBridge)getBridge();

        try {
            int deviceId = Integer.valueOf(getProperties().getId());
            int dataAddress = Integer.valueOf(getProperties().getChannelId());

            BitVector inputDiscretes = bridge.readInputDiscretes(deviceId, dataAddress, 1);

            ByteBuffer buff = ByteBuffer.allocate(4);
            buff.put(inputDiscretes.getBytes()[0]);

            int val = (int)ByteBuffer.wrap(buff.array()).get();

            if (getProperties().isInvert()) {
                return UnitValue.valueOf((val < 1 ? 1 : 0));
            } else {
                return UnitValue.valueOf(val < 1 ? 0 : 1);
            }

        } catch (Exception e) {
            throw new BridgeException("Can't get value of device ID:"+getProperties().getId());
        }
    }


    public void sendValue(UnitValue value) throws BridgeException {

    }
}
