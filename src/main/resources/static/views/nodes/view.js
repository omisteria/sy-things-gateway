'use strict';

angular.module('myApp.views.nodes', ['ngRoute', 'highcharts-ng', 'ui.codemirror'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/nodes', {
    templateUrl: 'views/nodes/view.html',
    controller: 'NodesCtrl'
    });
}])

.filter('matchTypes', function() {
    return function( items, showTypes) {
        var filtered = [];
        angular.forEach(items, function(item) {
            if(
                (item.type == 'sensor' && showTypes[0])
                || (item.type == 'handler' && showTypes[1])
                || (item.type == 'logic' && showTypes[2])
            ) {
                if (showTypes[3]) {
                    if (item.selected) filtered.push(item);
                } else {
                    filtered.push(item);
                }
            }
        });
        return filtered;
    };
})

.controller('NodesCtrl', function($scope, $timeout, $filter, nodesService, wsSyncService, toastr, $uibModal) {

        $scope.currentNode;
        $scope.properties;
        $scope.newNodeId;

        $scope.showTypes = [true, true, true, false];

        var loadData = function() {
            nodesService.getAllNodes().then(
                function(data) {
                    $scope.allnodes = data;

                    _.each($scope.allnodes, function(el) {
                        el.chartData = angular.copy($scope.chartConfig);
                        el.chartData.title.text = 'Changes of '+el.name;
                        el.selected = false;

                        el.observed = _.filter($scope.allnodes, function(node) {
                            return el.observedNodesIds.indexOf(node.id)>-1;
                        });
                        el.observes = _.filter($scope.allnodes, function(node) {
                            return el.observesNodesIds.indexOf(node.id)>-1;
                        });
                    });
                }
            );
        };

        $scope.createNewRule = function() {
            if ($scope.newNodeId)
                nodesService.createRule($scope.newNodeId).then(
                    function(newNode) {
                        loadData();
                    }
                );
        };

        $scope.selectedToggle = function(node) {
            node.selected = !node.selected;
        };
        $scope.switchOn = function(node) {
            nodesService.saveNodeValue(node.id, 1).then(
                function(data) {
                }
            );
        };
        $scope.switchOff = function(node) {
            nodesService.saveNodeValue(node.id, 0).then(
                function(data) {
                }
            );
        };

        $scope.tabRuleSelected = function() {
            $scope.isRefreshEditor = true;
            $timeout(function() {$scope.isRefreshEditor = false;}, 200);
        };

        $scope.editorOptions = {
            lineWrapping : true,
            lineNumbers: true,
            mode: 'javascript',
            theme: 'duotone-light'
        };

        loadData();

        /*$scope.aceLoaded = function(_editor){
            ace.require("ace/ext/language_tools");

            //_editor.setTheme("ace/theme/github");
            //_editor.getSession().setMode("ace/mode/javascript");

            _editor.setOptions({
                useWrapMode: true,
                showGutter: true,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true
            });
        };*/

        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        $scope.chartConfig = {
            options: {
                chart: {
                    height: 360,
                    width: 1180
                },
                xAxis: {
                    type: 'datetime',
                    ordinal: false
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            stops: [
                                [0, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 3
                            }
                        },
                        threshold: null
                    }
                }
            },
            series: [{
                step: true,
                data: []
            }],
            title: {
                text: 'Title'
            },
            loading: false
        };

        $scope.setCurrentNode = function(node) {

            node.expanded = node.expanded ? false : true;

            $scope.currentNode = node;

            nodesService.getNodeProperties(node.id).then(
                function(data) {
                    $scope.properties = data;
                }
            );
            nodesService.getNodeValueHistory(node.id).then(
                function(historyData) {
                    var series = node.chartData.series[0];
                    angular.forEach(_.sortBy(historyData, function(o) { return o.regdate; }), function(row) {
                        series.data.push({x:row.regdate, y:parseFloat(row.value)});
                    });
                }
            );
        };
        $scope.getNodeTypeClass = function(node) {
            return "c-icon-"+node.type;
        };

        $scope.saveScript = function(node) {
            nodesService.saveRuleScript(node.id, node.nodeDescription.script).then(
                function(response) {
                    toastr.success("Rule has been saved successfully", "Rule");
                    node.initialized = true;
                },
                function(error) {
                    toastr.error("Rule has not been saved", "Rule");
                    node.initialized = false;
                }
            );
        };
        $scope.addObservedNode = function(node) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'addNodeLink.html',
                controller: 'AddNodeLinkCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    node: function () {return node;},
                    nodes: function () {return $scope.allnodes;}
                }
            });

            modalInstance.result.then(
                function (link) {
                    nodesService.getAllNodeLinksOfNode(node.id).then(
                        function(links) {
                            node.observedNodesIds = _.map(links, function(el) {return el.observedId});

                            var observedNode = _.filter($scope.allnodes, function(el) {return el.id == link})[0];
                            observedNode.observesNodesIds.push(node.id);

                            node.observed = _.filter($scope.allnodes, function(n) {
                                return node.observedNodesIds.indexOf(n.id)>-1;
                            });
                            observedNode.observes = _.filter($scope.allnodes, function(n) {
                                return observedNode.observesNodesIds.indexOf(n.id)>-1;
                            });
                        }
                    );
                }
            );
        };
        $scope.deleteObserved = function(node, observedId) {
            nodesService.removeNodeLink(node.id, observedId).then(
                function() {
                    nodesService.getAllNodeLinksOfNode(node.id).then(
                        function(links) {
                            node.observedNodesIds = _.map(links, function(el) {return el.observedId});

                            var observedNode = _.filter($scope.allnodes, function(el) {return el.id == observedId})[0];
                            observedNode.observesNodesIds = _.filter(observedNode.observesNodesIds, function(el) {return el != node.id});

                            node.observed = _.filter($scope.allnodes, function(n) {
                                return node.observedNodesIds.indexOf(n.id)>-1;
                            });
                            observedNode.observes = _.filter($scope.allnodes, function(n) {
                                return observedNode.observesNodesIds.indexOf(n.id)>-1;
                            });
                        }
                    );
                    toastr.success("Observed has been removed successfully", "Node Link");
                },
                function() {
                    toastr.error("Observed has not been removed", "Node Link");
                }
            );
        };



        $scope.options = {
            renderer: 'line',
            interpolation: 'linear'
        };
        $scope.features = {
            xAxis: {
                timeUnit: 'minute'
            },
            yAxis: {

            },
            hover: {
                /*xFormatter: function(x) {
                    return 'T:' + moment(x).format("dddd, MMMM Do YYYY, h:mm");
                },
                yFormatter: function(y) {
                    return null;
                },*/
                formatter: function(obj, x, y) {
                    return moment(x).format("dddd, MMMM Do YYYY, HH:mm")+' = '+y;
                }
            }
        };

    wsSyncService.receive().then(null, null, function(node) {
        //console.log(node);
        _.each($scope.allnodes, function(el) {
            if (el.id === node.id) {
                el.value = node.value;
                el.timeOfLastUpdate = node.timeOfLastUpdate;
                el.initialized = node.initialized;

                var series = el.chartData.series[0];
                if (series.data.length<35) {
                    series.data.push({x:node.timeOfLastUpdate, y:node.value});
                } else {
                    series.data.shift();
                    series.data.push({x:node.timeOfLastUpdate, y:node.value});
                }
            }
        });
    });
})

.controller('AddNodeLinkCtrl', function ($uibModalInstance, nodesService, toastr, node, nodes) {
    var $ctrl = this;
    $ctrl.nodes = nodes;
    $ctrl.observed;

    $ctrl.ok = function () {
        if ($ctrl.observed) {
            nodesService.addNodeLink(node.id, $ctrl.observed).then(
                function() {
                    toastr.success("Observed has been saved successfully", "Node Link");
                    $uibModalInstance.close($ctrl.observed);
                },
                function() {
                    toastr.error("Observed has not been saved", "Node Link");
                }
            );
        }
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});