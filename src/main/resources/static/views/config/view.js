'use strict';

angular.module('myApp.views.config', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/config', {
            templateUrl: 'views/config/view.html',
            controller: 'ConfigCtrl'
        });
    }])

    .controller('ConfigCtrl', function($scope, nodesService) {

        $scope.devicesScript;
        $scope.logicNodesScript;

        var loadData = function() {
            nodesService.loadConfig().then(
                function(response) {
                    $scope.devicesScript = response.devicesConfig;
                    $scope.logicNodesScript = response.logicConfig;
                }
            );
        };

        loadData();

        $scope.saveConfig = function() {
            nodesService.saveConfig($scope.devicesScript, $scope.logicNodesScript).then(
                function(response) {
                    console.log('response: ' + response);
                }
            );
        }
    });