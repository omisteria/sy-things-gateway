'use strict';

angular.module('myApp.views.dash', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/dash', {
            templateUrl: 'views/dash/view.html',
            controller: 'DashCtrl'
        });
    }])

    .controller('DashCtrl', function($scope, $interval) {



    });