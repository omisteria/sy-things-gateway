'use strict';

angular.module('myApp.views.log', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/log', {
    templateUrl: 'views/log/view.html',
    controller: 'LogCtrl'
  });
}])

.controller('LogCtrl', function($scope, $interval, loggingService, wsSyncService) {

    var loadData = function() {
        loggingService.getLastLogRecords().then(
            function(data) {
              $scope.records = data;
            }
        );
    };

    loadData();
    //$interval(loadData, 30000);

    wsSyncService.receiveEvents().then(null, null, function(event) {
        console.log(event);
        if (event.type === "LOGGING_LIST") {
            loadData();
        }
    });
});