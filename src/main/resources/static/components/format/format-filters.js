/**
 * Created by Igor on 5/11/2015.
 */
'use strict';

angular.module('myApp.format.format-filters', [])

    .filter('strMonthYear', [function() {
        return function(ts) {
            var mo = moment.utc(ts);
            var localTz = moment().zone();
            mo.zone(localTz);
            return mo.format("MM.YYYY");
        };
    }])

    .filter('strTime', [function() {
        return function(ts) {
            var mo = moment.utc(ts);
            var localTz = moment().zone();
            mo.zone(localTz);
            return mo.format("HH:mm:ss");
        };
    }])

    .filter('strDate', [function() {
        return function(ts) {
            var mo = moment.utc(ts);
            var localTz = moment().zone();
            mo.zone(localTz);
            return mo.format("DD");
        };
    }]);

