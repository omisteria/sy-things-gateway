angular.module('myApp')

    .service("wsSyncService", function($q, $timeout) {

        var service = {}, socket = {
            client: null,
            stomp: null
        };
        var listener = $q.defer();
        var listenerEvents = $q.defer();

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/wssync";
        service.NODEVALUE_TOPIC = "/topic/nodevalue";
        service.EVENTS_TOPIC = "/topic/events";
        service.BROKER = "/wssync";

        service.receive = function() {
            return listener.promise;
        };

        service.receiveEvents = function() {
            return listenerEvents.promise;
        };

        service.send = function(message) {
            socket.stomp.send(service.BROKER, {
                priority: 9
            }, JSON.stringify({
                message: message,
                id: 100
            }));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        var startListener = function() {
            socket.stomp.subscribe(service.NODEVALUE_TOPIC, function(data) {
                listener.notify(JSON.parse(data.body));
            });
            socket.stomp.subscribe(service.EVENTS_TOPIC, function(data) {
                listenerEvents.notify(JSON.parse(data.body));
            });
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
            socket.stomp.debug = null;
        };

        initialize();

        return service;
    });