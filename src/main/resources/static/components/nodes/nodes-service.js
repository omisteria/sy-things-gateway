/**
 * Created by Igor on 5/12/2015.
 */
'use strict';

angular.module('myApp.nodes.nodes-service', [])

    .service('nodesService', function ($http, $q, $filter, serverInstance) {

        var handleRequestResult = function(promise) {
            var deferred = $q.defer();
            promise.then(
                function(response) {
                    deferred.resolve(response.data);
                },
                function() {
                    deferred.reject("Service error");
                }
            );
            return deferred.promise;
        };

        this.getAllNodes = function () {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/all';
            return handleRequestResult($http.get(actionURL));
        };

        this.getNodeProperties = function (nodeId) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + nodeId;
            return handleRequestResult($http.get(actionURL));
        };

        this.createRule = function (nodeId) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + nodeId;
            return handleRequestResult($http.post(actionURL));
        };

        this.saveRuleScript = function (ruleId, script) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + ruleId + '/script';
            return handleRequestResult($http.post(actionURL, script));
        };

        this.addNodeLink = function (nodeId, observedId) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + nodeId + '/addlink/' + observedId;
            return handleRequestResult($http.put(actionURL));
        };

        this.removeNodeLink = function (nodeId, observedId) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + nodeId + '/removelink/' + observedId;
            return handleRequestResult($http.put(actionURL));
        };

        this.getAllNodeLinksOfNode = function (nodeId) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/' + nodeId + '/nodelinks';
            return handleRequestResult($http.get(actionURL));
        };

        this.getNodeValueHistory = function(nodeId) {
            var dateFormat = "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]";
            var mStartDate = moment().subtract(2, 'months');
            var mEndDate = moment();
            var p = [];
            p.push("source=thingsgateway");
            p.push("unit="+nodeId);
            p.push("startDate="+mStartDate.format(dateFormat));
            p.push("endDate="+mEndDate.format(dateFormat));
            p.push("offset=0");
            p.push("limit=200");

            var actionURL = 'http://192.168.3.10:28380/api/events?' + p.join('&');
            return handleRequestResult($http.get(actionURL));
        };

        this.getNodeScript = function (nodeId) {
            var actionURL = serverInstance.apiURL + '/node/'+nodeId+'/script';

            var deferred = $q.defer();
            $http
                .get(actionURL)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(data){
                    deferred.reject("Service error");
                });
            return deferred.promise;
        };

        this.saveNodeScript = function (nodeScript) {
            var actionURL = serverInstance.apiURL + '/nodes/script';

            var deferred = $q.defer();
            $http
                .post(actionURL, nodeScript)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(data){
                    deferred.reject("Service error");
                });
            return deferred.promise;
        };

        this.saveNodeValue = function (nodeId, value) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/nodes/one/'+nodeId+'/value/'+value;
            return handleRequestResult($http.post(actionURL));
        };



        this.loadConfig = function () {
            var actionURL = serverInstance.apiURL + '/config';

            var deferred = $q.defer();
            $http
                .get(actionURL)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(data){
                    deferred.reject("Service error");
                });
            return deferred.promise;
        };

        this.saveConfig = function (devicesConfig, logicConfig) {
            var actionURL = serverInstance.apiURL + '/config';

            var data = {
                devicesConfig: devicesConfig,
                logicConfig: logicConfig
            };

            var deferred = $q.defer();
            $http
                .post(actionURL, data)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(data){
                    deferred.reject("Service error");
                });
            return deferred.promise;
        };

        return this;
    });