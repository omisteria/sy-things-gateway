/**
 * Created by Igor on 5/11/2015.
 */
'use strict';

angular.module('myApp.logging.logging-service', [])

    .service('loggingService', function ($http, $q, $filter, serverInstance) {


        var handleRequestResult = function(promise) {
            var deferred = $q.defer();
            promise.then(
                function(response) {
                    deferred.resolve(response.data);
                },
                function() {
                    deferred.reject("Service error");
                }
            );
            return deferred.promise;
        };

        this.getLastLogRecords = function (host, sender) {
            var actionURL = serverInstance.apiURL + serverInstance.apiVersion + '/log/all/60';
            return handleRequestResult($http.get(actionURL));
        };

        return this;
});
