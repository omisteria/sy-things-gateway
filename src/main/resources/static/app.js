'use strict';

angular
    .module('myApp', [
      'ngRoute',
      'ngAnimate',
      'ui.bootstrap',
      'myApp.logging',
      'myApp.nodes',
      'myApp.format',
      'myApp.views.nodes',
      'myApp.views.log',
      'toastr'
    ])

    .config(function($routeProvider, $httpProvider) {

        $routeProvider
             .otherwise({redirectTo: '/nodes'});

    })

    .constant("serverInstance", (function() {
      var protocol = "http://";
      var host = "192.168.3.10";
      //var host = "localhost";
      var port = "28480";
      //var port = "8080";
      var context = "/";
      return {
        baseURL: protocol + host + ':' +  port + context,
        apiURL: protocol + host + ':' + port + context + 'api',
        apiVersion: '/v1'
      }
    })())

    .controller('globalController', function($scope) {

    });
