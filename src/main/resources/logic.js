/**
 * Условное начало и конец дня
 */
node(
    "uDayIsBeginninig", function() {
        var timeScheduler1 = createCron(function() {
            uDayIsBeginninig.value = 1;
        }, '0 30 5 * * ?');
        var timeScheduler2 = createCron(function() {
            uDayIsBeginninig.value = 0;
        }, '0 30 2 * * ?');
    },
    [], function() {
    }
);

/**
 * Отслеживание состояние нижнего замка входной двери
 */
node(
    "uOpenedBottomDoorLock", function() {
        var timer = setInterval(function() {
            if (sensorBottomDoorLock.value != 1) {
                var h = new Date().getHours();
                if ( (h>=0 && h<=4) || (h>=17 && h<=23) ) {

                    sayText("Зам+ок входной двери открыт");

                }
            }
        }, 240); // 4min

        //uOpenedBottomDoorLock.context.set("timer", timer);
    },
    ["sensorBottomDoorLock"], function() {

    }
);


/**
 * Статус 'Никого нет дома'
 */
node(
    "uNoAnybodyHome", function() {

    },
    ["sensorTopDoorLock"], function() {

        if (sensorTopDoorLock.value > 0) {

            setTimeout(function () {
                uNoAnybodyHome.value = 1;
            }, 120); // 2min

        } else {
            uNoAnybodyHome.value = 0;
        }

    }
);

/**
 * Отслеживание присутсвия в холле
 */
node(
    "uAnybodyInHall", function() {
    },
    ["sensorMovementHall"], function() {
        if (sensorMovementHall.value>0) {
            uAnybodyInHall.value = 1;
            var timer = uAnybodyInHall.context.get("timer");

            if (timer) clearTimeout(timer);

            uAnybodyInHall.context.set("timer", setTimeout(function() {
                uAnybodyInHall.value = 0;
            }, 1200)); // 20 min
        }
    }
);


/**
 * Повышенное потребление электроэнергии больше 10 мин
 */
node(
    "uHighElectricityCons", function() {

    },
    ["electricityActivePower"], function() {
        if (electricityActivePower.value>2600) { // more 2.6kWt

            uHighElectricityCons.value = 1;
            var timer = uHighElectricityCons.context.get("timer");

            if (timer) clearTimeout(timer);

            uHighElectricityCons.context.set("timer", setTimeout(function() {
                uHighElectricityCons.value = 0;
            }, 600)); // 10 min
        }
    }
);

/**
 * Предположительно включена духовка
 */
node(
    "uOvenIsOn", function() {

    },
    ["uHighElectricityCons"], function() {
        var timer = uOvenIsOn.context.get("timer");
        if (timer) clearTimeout(timer);
        uOvenIsOn.value = 0;

        if (uHighElectricityCons.value>0) {
            uOvenIsOn.context.set("timer", setTimeout(function() {
                uOvenIsOn.value = 1;
            }, 660)); // 11 min
        }
    }
);

/**
 * Включение потолочного вентилятора кухни
 */
node(
    "uKitchenCeilingVentilator", function() {

    },
    ["uOvenIsOn","uAnybodyInHall"], function() {

        if (uHighElectricityCons.value>0 && uAnybodyInHall.value>0) {
            var timer = uKitchenCeilingVentilator.context.get("timer");
            if (timer) clearTimeout(timer);

            uKitchenCeilingVentilator.value = 1;
            switchKitchenFloorHeater.value = 1;
            uKitchenCeilingVentilator.context.set("timer", setTimeout(function() {
                uKitchenCeilingVentilator.value = 0;
                switchKitchenFloorHeater.value = 0;
            }, 3600)); // 60 min
        }
    }
);

/**
 * Приветствие при входе домой
 */
node(
    "uOpenedTopDoorLock", function() {
        uOpenedTopDoorLock.context.set("flagGreeted", false);
    },
    ["uNoAnybodyHome"], function() {

        var flagGreeted = uOpenedTopDoorLock.context.get("flagGreeted");

        if (uNoAnybodyHome.value > 0) {

            sayText("Кажется никого нет дома");
            uOpenedTopDoorLock.context.set("flagGreeted", true);

        } else {
            if (flagGreeted) {

                setTimeout(function () {
                    sayText("Добро пожаловать домой");
                    uOpenedTopDoorLock.context.set("flagGreeted", false);
                }, 15); // 15sec

            }
        }

    }
);

/**
 * Отслеживание открытых окон для вентиляции на северной стороне
 * если активен - принудительную вентиляцию отключаем
 */
node(
    "uOpenWindowsLongtime", function() {
    },
    ["sensorOpenWindNorth"], function() {
        var timer = uOpenWindowsLongtime.context.get("timer");

        if (sensorOpenWindNorth.value>0) {


            if (timer) clearTimeout(timer);

            uOpenWindowsLongtime.context.set("timer", setTimeout(function() {
                uOpenWindowsLongtime.value = 1;

                var h = new Date().getHours();
                if (h>=6 && h<=23) sayText("Отключаю вентиляцию");

            }, 600)); // 10 min
        } else {
            if (timer) clearTimeout(timer);
            uOpenWindowsLongtime.value = 0;
        }
    }
);

/**
 * Генератор дискретного уровня для вентилятора вентиляции
 */
node(
    "uAirFlow", function() {
        var tv, bv = 1;
        var timer = setInterval(function() {
            var topval = uAirFlow.context.get("topval") || 1;
            var botval = uAirFlow.context.get("botval") || 1;
            if (tv > 0) { tv--; uAirFlow.value = 1; } else if (bv > 0) { bv--; uAirFlow.value = 0; if (bv===0) {tv=topval; bv=botval;}}

            log("Set current flow to " + topval + "." + botval + " for [uAirFlow]");
        }, 300); // 5min

        uAirFlow.context.set("timer", timer);
    },
    ["tempBedroom","tempOutdoor","humidityBedroom"], function() {
        var topc = createConverter(0.5,1,1,6);
        var botc = createConverter(0,0.5,6,1);

        var inHumidity = createConverter(45,75,1,1.4);
        var inTemp = createConverter(21,23,0.27,1);
        var outTempHot = createConverter(24,32,1,0.2);
        var outTempCold = createConverter(-15,4,0.3,1);

        var tempInside = tempBedroom.value - 1.5;

        var k1 = inTemp.convert(tempInside);
        var k2 = outTempHot.convert(tempOutdoor.value);
        var k3 = outTempCold.convert(tempOutdoor.value);
        var k4 = inHumidity.convert(humidityBedroom.value);
        var level = k1 * k2 * k3 * k4;

        log("Set level[uAirFlow] to "+level+", inside temp "+tempInside+" is "+k1+", outside very hot temp "+tempOutdoor.value+" is "+k2+", outside very cold temp "+tempOutdoor.value+" is "+k3+", inside humidity "+humidityBedroom.value+" is "+k4);

        uAirFlow.context.set("topval", parseInt(topc.convert(level)));
        uAirFlow.context.set("botval", parseInt(botc.convert(level)));
    }
);

/**
 * Отслеживание разницы внутренних и внешних температур
 * для вентиляции в летнее время
 */
node(
    "uDiffTemperature", function() {
    },
    ["tempBedroom","tempOutdoor","humidityBedroom"], function() {

        var tempInside = tempBedroom.value - 1.5;
        var diff = tempInside - tempOutdoor.value;

        uDiffTemperature.value = (tempInside > 23 && humidityBedroom.value>45 && diff > 0 ? 1 : 0);

        if (uDiffTemperature.value > 0) {
            log("Diff temperatures is "+diff+"C and "+humidityBedroom.value+"% humidity, enable additional cooler time");
        }
    }
);

/**
 * Контроль вентилятора вентиляции
 */
node(
    "uAirCooler", function() {
    },
    ["uAirFlow","uOpenWindowsLongtime","uDiffTemperature","uNoAnybodyHome"], function() {

        if (uOpenWindowsLongtime.value > 0 || uNoAnybodyHome.value == 1) {
            uAirCooler.value = 0;
        } else {
            uAirCooler.value = (uAirFlow.value + uDiffTemperature.value > 0 ? 1 : 0);
        }

        switchVentCooler.value = uAirCooler.value;
    }
);

/**
 * Предупреждение о лишней потери тепла через открытые окна
 */
node(
    "uHeatLossDetector", function() {
    },
    ["tempHall","sensorOpenWindSouth","sensorOpenWindNorth"], function() {

        if ((tempHall.value < 21.5 || tempBedroom.value < 21.5)
            && (sensorOpenWindSouth.value>0 || sensorOpenWindNorth.value>0)) {

            uHeatLossDetector.value = 1;
        } else {
            uHeatLossDetector.value = 0;
        }
    }
);
node(
    "uHeatLoss", function() {
    },
    ["uHeatLossDetector"], function() {

        var timer = uHeatLoss.context.get("timer");
        if (timer) clearTimeout(timer);

        if (uHeatLossDetector.value>0) {

            uHeatLoss.context.set("timer", setTimeout(function() {
                uHeatLoss.value = 1;
                sayText("Очень х+олодно предлагаю закрыть +окна");
            }, 60)); // 1 min

        } else {

            //if (uHeatLoss.value>0) {
            //    uHeatLoss.value = 0;

            //    sayText("Спасибо");
            //}
        }
    }
);

/**
 * Контроль нагревателя вентиляции
 */
node(
    "uAirHeater", function() {
    },
    ["tempOutdoor"], function() {
        switchVentHeater.value = tempOutdoor.value < 4 ? 1 : 0;
    }
);



/**
 * Включение подогрева пола на кухне в 6:25 в рабочие дни
 */
node(
    "uKitchenFloorHeaterSchedul", function() {
        var timeScheduler = createCron(function() {
            //var timeScheduler = createTimepoint(function() {}, '6:33');

            uKitchenFloorHeaterSchedul.value = 1;

            setTimeout(function () {
                uKitchenFloorHeaterSchedul.value = 0;
            }, 2400); // 40min

        }, '0 25 6 ? * MON-FRI');
    },
    [], function() {

    }
);

/**
 * Решение о включение подогрева пола
 */
node(
    "uKitchenFloorHeaterDecision", function() {

    },
    ["uKitchenFloorHeaterSchedul","uNoAnybodyHome","tempOutdoor","tempHall"], function() {

        if (uKitchenFloorHeaterSchedul.value > 0 && uNoAnybodyHome.value != 1 && tempOutdoor.value < 8 && tempHall.value < 23.1) {

            switchKitchenFloorHeater.value = 1;

        } else {

            switchKitchenFloorHeater.value = 0;

        }

    }
);

/**
 * Отслеживание открытых окон, предупреждение о сквозняке
 */
node(
    "uOpenWindowOppositeSides", function() {
    },
    ["sensorOpenWindSouth","sensorOpenWindNorth"], function() {

        if (sensorOpenWindSouth.value>0 && sensorOpenWindNorth.value>0) {
            uOpenWindowOppositeSides.value = 1;

            var h = new Date().getHours();
            if (h>=6 && h<=23) sayText("Внимание! возможен скъвозняк");
        } else {
            uOpenWindowOppositeSides.value = 0;
        }
    }
);


/**
 * Отслеживание времени, начало каждого часа
 */
node(
    "uTimeObserver", function() {
        var timeScheduler1 = createCron(function() {
            uTimeObserver.value = 1;
        }, '0 0 * * * ?');
        var timeScheduler2 = createCron(function() {
            uTimeObserver.value = 0;
        }, '0 1 * * * ?');
    },
    [], function() {
    }
);

/**
 * Оглашение времени
 */
node(
    "uTimeSpeak", function() {
    },
    ["uTimeObserver"], function() {
        if (uTimeObserver.value>0) {
            var h = new Date().getHours();
            if (h==19) return; //TODO привязать пропуски к календарю
            if (h>10 && h<=20) sayText("Время "+h+" часов");
            if (h==21) sayText("Время "+h+" час");
            if (h>=22 && h<=23) sayText("Время "+h+" часа");

            if (h==23) {
                setTimeout(function () {
                    sayText("Спокойной ночи");
                }, 10); // 10sec
            }
        }
    }
);

/**
 * Утреннее приветствие
 */
node(
    "uGoodMorning", function() {
        uGoodMorning.context.set("flagGreeted", false);
    },
    ["uAnybodyInHall", "uDayIsBeginninig"], function() {
        var flagGreeted = uGoodMorning.context.get("flagGreeted");
        var h = new Date().getHours();

        if (uAnybodyInHall.value>0) {
            if (uDayIsBeginninig.value>0) {
                if (!flagGreeted && h<12) {
                    var temperatureOut = parseInt(tempOutdoor.value);
                    //var temperatureIn = parseInt(tempHall.value - 1.5);
                    //var pressure = parseInt(pressure.value);
                    //sayText("Доброе утро. Температура на улице "+temperatureOut+" градуса, в помещении "+temperatureIn+" градуса, давление "+pressure+" милиметров. Хорошего дня");

                    //var text = "Доброе утро Температура на улице "+temperatureOut+" градуса в помещении "+temperatureIn+" градуса  Хорошего дня";
                    var text = "Доброе утро Температура на улице "+temperatureOut+" градуса";
                    log(text);
                    sayText(text);
                }
                uGoodMorning.context.set("flagGreeted", true);
            } else {
                uGoodMorning.context.set("flagGreeted", false);
            }
        }
    }
);