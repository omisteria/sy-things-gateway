/**
 * Генератор дискретного уровня для вентилятора вентиляции
 */
node(
    "uAirFlow", function() {
        var tv, bv = 1;
        setInterval(function() {
            var topval = uAirFlow.context.get("topval") || 1;
            var botval = uAirFlow.context.get("botval") || 1;
            if (tv > 0) { tv--; uAirFlow.value = 1; } else if (bv > 0) { bv--; uAirFlow.value = 0; if (bv==0) {tv=topval; bv=botval;}}

            log("Set current flow to " + topval + "." + botval + " for [uAirFlow]");
        }, 300); // 5min
    },
    ["tempBedroom","tempOutdoor"], function() {
        var topc = createConverter(0.5,1,1,6);
        var botc = createConverter(0,0.5,6,1);
        var inTemp = createConverter(18.7,23.5,0,1);
        var outTempHot = createConverter(26,37,1,0.2);
        var outTempCold = createConverter(-25,4,0.3,1);
        var tempInside = tempBedroom.value - 1.5;

        var level = inTemp.convert(tempInside)
            * outTempHot.convert(tempOutdoor.value)
            * outTempCold.convert(tempOutdoor.value);

        log("Set level to "+level+" for [uAirFlow]");
        uAirFlow.context.set("topval", parseInt(topc.convert(level)));
        uAirFlow.context.set("botval", parseInt(botc.convert(level)));
    }
);

/**
 * Отслеживание открытых окон для вентиляции на северной стороне
 * если активен - принудительную вентиляцию отключаем
 */
node(
    "uOpenWindowsLongtime", function() {
    },
    ["sensorOpenWindNorth"], function() {
        var timer = uOpenWindowsLongtime.context.get("timer");

        if (sensorOpenWindNorth.value>0) {


            if (timer) clearTimeout(timer);

            uOpenWindowsLongtime.context.set("timer", setTimeout(function() {
                uOpenWindowsLongtime.value = 1;

                sayText("Отключаю вентиляцию");

            }, 600)); // 10 min
        } else {
            if (timer) clearTimeout(timer);
            uOpenWindowsLongtime.value = 0;
        }
    }
);

/**
 * Контроль вентилятора вентиляции
 */
node(
    "uAirCooler", function() {
    },
    ["uAirFlow","uOpenWindowsLongtime"], function() {

        if (uOpenWindowsLongtime.value > 0) {
            uAirCooler.value = 0;
        } else {
            uAirCooler.value = uAirFlow.value;
        }

        switchVentCooler.value = uAirCooler.value;
    }
);

/**
 * Контроль нагревателя вентиляции
 */
node(
    "uAirHeater", function() {
    },
    ["tempOutdoor"], function() {
        switchVentHeater.value = tempOutdoor.value < 4 ? 1 : 0;
    }
);

/**
 * Отслеживание присутсвия в холле
 */
node(
    "uAnybodyInHall", function() {
    },
    ["sensorMovementHall"], function() {
        if (sensorMovementHall.value>0) {
            uAnybodyInHall.value = 1;
            var timer = uAnybodyInHall.context.get("timer");

            if (timer) clearTimeout(timer);

            uAnybodyInHall.context.set("timer", setTimeout(function() {
                uAnybodyInHall.value = 0;
            }, 300)); // 5 min
        }
    }
);

/**
 * Отслеживание открытых окон, предупреждение о сквозняке
 */
node(
    "uOpenWindowOppositeSides", function() {
    },
    ["sensorOpenWindSouth","sensorOpenWindNorth"], function() {

        if (sensorOpenWindSouth.value>0 && sensorOpenWindNorth.value>0) {
            uOpenWindowOppositeSides.value = 1;
            sayText("Внимание! возможен скъвозняк");
        } else {
            uOpenWindowOppositeSides.value = 0;
        }
    }
);

/**
 *
 */
node(
    "uKitchenFloorHeater", function() {
        var timeScheduler = createCron(function() {
            //var timeScheduler = createTimepoint(function() {}, '6:33');

            if (tempOutdoor.value < 9) {

                switchKitchenFloorHeater.value = 1;

                setTimeout(function () {
                    switchKitchenFloorHeater.value = 0;
                }, 2400); // 40min
            }

        }, '0 25 6 ? * MON-FRI');
        //uTimePoint.context.set("timeScheduler", timeScheduler);
    },
    [], function() {
        //var timeScheduler = uTimePoint.context.get("timeScheduler");

        // get expression from property

        //timeScheduler.reschedule('0/3 * * * * ?');
    }
);