var Timer = Java.type("java.util.Timer")

function setTimerRequest(handler, delay, interval, args) {
    handler = handler || function() {};
    delay = delay || 0;
    interval = interval || 0;

    var applyHandler = function() {handler.apply(this, args);}
    //var runLater = function() Platform.runLater(applyHandler);

    var timer = new Timer("setTimerRequest", true);

    if (interval > 0) {
        //timer.schedule(runLater, delay, interval);
        timer.schedule(applyHandler, delay * 1000, interval * 1000)
    } else {
        //timer.schedule(runLater, delay);
        timer.schedule(applyHandler, delay * 1000);
    }

    sys.registerTimer(timer);
    return timer;
}

function clearTimerRequest(timer) {
    timer.cancel();
}

function setInterval() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();

    return setTimerRequest(handler, ms, ms, args);
}

function clearInterval(timer) {
    clearTimerRequest(timer);
}

function setTimeout() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();

    return setTimerRequest(handler, ms, 0, args);
}

function clearTimeout(timer) {
    clearTimerRequest(timer);
}

function setImmediate() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();

    return setTimerRequest(handler, 0, 0, args);
}

function clearImmediate(timer) {
    clearTimerRequest(timer);
}



function sayText(text) {
    sys.sayText(text);
}
function rule(constructor, handler) {
    sys.rule(constructor, handler);
}
function log(message) {
    sys.log(message);
}

function createCron(handler, expression) {
    return sys.cron(handler, expression);
}
function createTimepoint(handler, expression) {
    return sys.timepoint(handler, expression);
}
function clearScheduler(scheduler) {
    return sys.clearScheduler(scheduler);
}

function createConverter(fromDown, fromUp, toDown, toUp) {

    function converter(fromDown, fromUp, toDown, toUp) {
        var _this = this;
        this.fromDown = fromDown;
        this.fromUp = fromUp;
        this.toDown = toDown;
        this.toUp = toUp;

        this.convert = function(inputValue){
            var per = 0;
            if (_this.fromDown < _this.fromUp) {
                if (inputValue < _this.fromDown) {
                    inputValue = _this.fromDown;
                } else if (inputValue > _this.fromUp) {
                    inputValue = _this.fromUp;
                }

                per = (inputValue - _this.fromDown)/(_this.fromUp - _this.fromDown);
            } else {
                if (inputValue < _this.fromUp) {
                    inputValue = _this.fromUp;
                } else if (inputValue > _this.fromDown) {
                    inputValue = _this.fromDown;
                }

                per = (inputValue - _this.fromUp)/(_this.fromDown - _this.fromUp);
            }

            if (_this.toDown < _this.toUp) {
                return _this.toDown + (_this.toUp - _this.toDown) * per;
            } else {
                return _this.toDown - (_this.toDown - _this.toUp) * per;
            }
        }
    }

    return new converter(fromDown, fromUp, toDown, toUp);
}


