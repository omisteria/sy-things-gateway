CREATE SEQUENCE seq_thing_description
  INCREMENT 1
  MINVALUE 1
  START 1;
ALTER TABLE seq_thing_description OWNER TO postgres;

CREATE TABLE thing_description
(
  descriptor_id integer NOT NULL DEFAULT nextval('seq_thing_description'::regclass),
  gid character varying(50) NOT NULL,
  name character varying(230) NOT NULL,
  type character varying(50) NOT NULL,
  polling integer NULL,
  invert boolean NOT NULL DEFAULT false
  options character varying(450) NOT NULL,
  java_class text NOT NULL,
  CONSTRAINT pk_thing_description PRIMARY KEY (descriptor_id),
  CONSTRAINT uq_thing_description_gid UNIQUE (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE thing_description
  OWNER TO postgres;

INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('buttonHallNum1', 'Button In Hall Number 1', 'sensor', null, false, 'ethVKMod#192.168.3.175#0', 'com.synapse.ms.things.bridge.ethvkmod.device.SensorSocket2');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('lightHallBra', 'Light In Hall, Bra', 'switch', null, false, 'ethVKMod#192.168.3.175#0', 'com.synapse.ms.things.bridge.ethvkmod.device.RelaySocket2');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('lightMainBathroom', 'Light In Bathroom, Main', 'switch', null, false, 'rs485tcp#2#0', 'com.synapse.ms.things.bridge.rs485tcp.device.CoilVKM');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('movementSensorBathroom', 'Movement Sensor Bathroom', 'sensor', 1, true, 'rs485tcp#2#0', 'com.synapse.ms.things.bridge.rs485tcp.device.DiscreteInputVKM');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorDoorBathroom', 'Sensor Door Bathroom', 'sensor', 1, true, 'rs485tcp#2#1', 'com.synapse.ms.things.bridge.rs485tcp.device.DiscreteInputVKM');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('electricityTotalPower', 'Electricity Total Power', 'sensor', 120, false, 'rs485tcp#1#72', 'com.synapse.ms.things.bridge.rs485tcp.device.TotalPowerSMD220');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('electricityActivePower', 'Electricity Active Power', 'sensor', 60, false, 'rs485tcp#1#12', 'com.synapse.ms.things.bridge.rs485tcp.device.InputRegisterSMD220');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('tempHall', 'Hall - Temperature', 'sensor', 180, false, 'ow#28.5B1372020000', 'com.synapse.ms.things.bridge.ownet.device.TempSensorDS1820');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('tempBedroom', 'Bedroom - Temperature', 'sensor', 180, false, 'ow#26.D209CC000000', 'com.synapse.ms.things.bridge.ownet.device.TempSensorDS2438');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('pressure', 'Pressure', 'sensor', 360, false, 'ow#26.D209CC000000', 'com.synapse.ms.things.bridge.ownet.device.PressureSensorDS2438');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('humidityBedroom', 'Bedroom - Humidity', 'sensor', 360, false, 'ow#26.D209CC000000', 'com.synapse.ms.things.bridge.ownet.device.HumiditySensorDS2438');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('tempOutdoor', 'Outdoor - Temperature', 'sensor', 180, false, 'ow#28.52EB71020000', 'com.synapse.ms.things.bridge.ownet.device.TempSensorDS1820');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('switchVentCooler', 'Ventilation Cooler', 'switch', null, false, 'ow#3A.D13B0D000000#A', 'com.synapse.ms.things.bridge.ownet.device.DoubleSwitchDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('switchVentHeater', 'Ventilation Heater', 'switch', null, false, 'ow#3A.D13B0D000000#B', 'com.synapse.ms.things.bridge.ownet.device.DoubleSwitchDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorMovementHall', 'Movement sensor of hall', 'sensor', 1, true, 'ow#3A.D62C02000000#B', 'com.synapse.ms.things.bridge.ownet.device.DoubleSensorDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorOpenWindSouth', 'Opened windows of south side', 'sensor', 1, false, 'ow#3A.150A0E000000#B', 'com.synapse.ms.things.bridge.ownet.device.DoubleSensorDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorOpenWindNorth', 'Opened windows of north side', 'sensor', 1, false, 'ow#3A.08870E000000#B', 'com.synapse.ms.things.bridge.ownet.device.DoubleSensorDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('switchKitchenTopVent', 'Kitchen ceiling ventilator', 'switch', null, false, 'ow#3A.312D02000000#A', 'com.synapse.ms.things.bridge.ownet.device.DoubleSwitchDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('switchKitchenFloorHeater', 'Kitchen Floor Heater', 'switch', null, false, 'ow#3A.DB7F0E000000#B', 'com.synapse.ms.things.bridge.ownet.device.DoubleSwitchDS2413');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorBottomDoorLock', 'Opened bottom door lock', 'sensor', 5, true, 'ow#29.185911000000#1', 'com.synapse.ms.things.bridge.ownet.device.SensorDS2408');
INSERT INTO thing_description(gid, name, type, polling, invert, options, java_class) VALUES('sensorTopDoorLock', 'Opened top door lock', 'sensor', 5, true, 'ow#29.185911000000#0', 'com.synapse.ms.things.bridge.ownet.device.SensorDS2408');


CREATE TABLE rules
(
  rule_id character varying(50) NOT NULL,
  description character varying(230) NOT NULL,
  listen_for character varying(400) NOT NULL,
  script_init text NOT NULL,
  script_main text NOT NULL,
  CONSTRAINT pk_rules PRIMARY KEY (rule_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rules
  OWNER TO postgres;


INSERT INTO rules(rule_id, description, listen_for, script_init, script_main)
VALUES('uDayIsBeginninig', 'Условное начало и конец дня', '', 'var timeScheduler1 = createCron(function() {
             uDayIsBeginninig.value = 1;
         }, ''0 30 5 * * ?'');
         var timeScheduler2 = createCron(function() {
             uDayIsBeginninig.value = 0;
         }, ''0 30 2 * * ?'');','');
INSERT INTO rules(rule_id, description, listen_for, script_init, script_main)
VALUES('uOpenedBottomDoorLock', 'Отслеживание состояние нижнего замка входной двери', 'sensorBottomDoorLock', '','var timer = uOpenedBottomDoorLock.context.get("timer");
   		if (timer) clearTimeout(timer);

		if (sensorBottomDoorLock.value != 1) {
			uOpenedBottomDoorLock.context.set("timer", setInterval(function() {
                var h = new Date().getHours();
                if ( (h>=0 && h<=4) || (h>=17 && h<=23) ) {
					sayText("Зам+ок входной двери открыт");
				}
			}, 240)); // 4min
		}');
INSERT INTO rules(rule_id, description, listen_for, script_init, script_main)
VALUES('uNoAnybodyHome', 'Статус ''Никого нет дома''', 'sensorTopDoorLock', '','if (sensorTopDoorLock.value > 0) {

             setTimeout(function () {
                 uNoAnybodyHome.value = 1;
             }, 120); // 2min

         } else {
             uNoAnybodyHome.value = 0;
         }');
INSERT INTO rules(rule_id, description, listen_for, script_init, script_main)
VALUES('uAnybodyInHall', 'Отслеживание присутсвия в холле', 'sensorMovementHall', '','if (sensorMovementHall.value>0) {
             uAnybodyInHall.value = 1;
             var timer = uAnybodyInHall.context.get("timer");

             if (timer) clearTimeout(timer);

             uAnybodyInHall.context.set("timer", setTimeout(function() {
                 uAnybodyInHall.value = 0;
             }, 1200)); // 20 min
         }');


CREATE SEQUENCE seq_logmessages
  INCREMENT 1
  MINVALUE 1
  START 1;
ALTER TABLE seq_logmessages OWNER TO postgres;

CREATE TABLE logmessages
(
  msg_id integer NOT NULL DEFAULT nextval('seq_logmessages'::regclass),
  create_time timestamp without time zone NOT NULL,
  message character varying(900) NOT NULL,
  CONSTRAINT pk_logmessages PRIMARY KEY (msg_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logmessages
  OWNER TO postgres;


CREATE SEQUENCE seq_node_links
  INCREMENT 1
  MINVALUE 1
  START 1;
ALTER TABLE seq_node_links OWNER TO postgres;

CREATE TABLE node_links
(
   link_id bigint NOT NULL DEFAULT nextval('seq_node_links'::regclass),
   node_id character varying(40) NOT NULL,
   observed_id character varying(40) NOT NULL,
   CONSTRAINT pk_link_id PRIMARY KEY (link_id)
)
WITH (
  OIDS = FALSE
);