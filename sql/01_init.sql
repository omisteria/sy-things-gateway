CREATE TABLE `nodevalue` (
  `node_id` VARCHAR(30) NOT NULL,
  `create_time` TIMESTAMP NOT NULL,
  `node_value` DOUBLE NOT NULL
);

CREATE TABLE `logmessages` (
  `msg_id` INT NOT NULL UNIQUE AUTO_INCREMENT,
  `create_time` TIMESTAMP NOT NULL,
  `level` VARCHAR(10) NOT NULL,
  `message` VARCHAR(900)
);

CREATE TABLE `logicnodes` (
  `node_id` VARCHAR(30) NOT NULL UNIQUE,
  `script` TEXT
);

CREATE TABLE `configscripts` (
  `name` VARCHAR(50) NOT NULL,
  `version` VARCHAR(25) NOT NULL,
  `script` TEXT NOT NULL,
  PRIMARY KEY (`name`, `version`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC, `version` ASC)
);